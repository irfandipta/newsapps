-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 05, 2014 at 12:28 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `newsapps`
--

-- --------------------------------------------------------

--
-- Table structure for table `ap_apps_icon`
--

CREATE TABLE IF NOT EXISTS `ap_apps_icon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `app_name` varchar(100) DEFAULT NULL,
  `app_short_name` varchar(100) DEFAULT NULL,
  `splash_screen_img` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `template_id` (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ap_apps_icon`
--

INSERT INTO `ap_apps_icon` (`id`, `template_id`, `logo`, `updated`, `app_name`, `app_short_name`, `splash_screen_img`) VALUES
(2, 2, 'http://localhost/yii/newsapps/images/apps_icon_template.png', '2014-09-05 10:06:02', 'Test Test', 'test', 'http://localhost/yii/newsapps/images/apps_splash_template.jpg');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ap_apps_icon`
--
ALTER TABLE `ap_apps_icon`
  ADD CONSTRAINT `ap_apps_icon_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `ap_template` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
