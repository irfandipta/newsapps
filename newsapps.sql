-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 08, 2014 at 11:23 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `newsapps`
--

-- --------------------------------------------------------

--
-- Table structure for table `ap_apps_icon`
--

CREATE TABLE IF NOT EXISTS `ap_apps_icon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `template_id` (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ap_apps_icon`
--

INSERT INTO `ap_apps_icon` (`id`, `template_id`, `logo`, `updated`) VALUES
(1, 3, 'http://tinyurl.com/pqs4n8o', '2014-08-05 06:06:27');

-- --------------------------------------------------------

--
-- Table structure for table `ap_categories`
--

CREATE TABLE IF NOT EXISTS `ap_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_icon` int(11) DEFAULT NULL,
  `sort_code` int(11) DEFAULT NULL,
  `category_name` varchar(50) DEFAULT NULL,
  `active_flag` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` varchar(20) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tid` int(11) DEFAULT NULL,
  `color_code` varchar(10) DEFAULT NULL,
  `parentID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ap_categories_0` (`id_icon`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ap_categories`
--

INSERT INTO `ap_categories` (`id`, `id_icon`, `sort_code`, `category_name`, `active_flag`, `created_by`, `created_date`, `tid`, `color_code`, `parentID`) VALUES
(1, 6, 1, 'News', 1, 'Vero', '2014-07-14 09:15:04', 1, '#110000', 0),
(2, 19, 2, 'Gossip', 1, 'Veronica', '2014-07-14 09:15:04', 2, '#010101', 1),
(3, 18, 3, 'News 2', 1, 'Vero', '2014-07-21 06:28:28', 3, '#000000', 0),
(4, 19, 4, 'ayam', 1, 'aaa', '2014-07-23 01:50:59', 4, '#000000', 3),
(5, 20, 5, 'aadadasd', 1, 'aaaa', '2014-07-23 01:51:48', 5, '#000000', 3);

-- --------------------------------------------------------

--
-- Table structure for table `ap_categories_bak`
--

CREATE TABLE IF NOT EXISTS `ap_categories_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_icon` int(11) DEFAULT NULL,
  `sort_code` int(11) DEFAULT NULL,
  `category_name` varchar(50) DEFAULT NULL,
  `active_flag` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` varchar(20) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ap_categories_0` (`id_icon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_editions`
--

CREATE TABLE IF NOT EXISTS `ap_editions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `edition_name` varchar(30) DEFAULT NULL,
  `sort_code` int(11) DEFAULT '0',
  `active_flag` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` varchar(20) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_editions_bak`
--

CREATE TABLE IF NOT EXISTS `ap_editions_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `edition_name` varchar(30) DEFAULT NULL,
  `sort_code` int(11) DEFAULT '0',
  `active_flag` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` varchar(20) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_editions_categories`
--

CREATE TABLE IF NOT EXISTS `ap_editions_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `edition_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ap_editions_categories` (`edition_id`),
  KEY `idx_ap_editions_categories_0` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_editions_categories_bak`
--

CREATE TABLE IF NOT EXISTS `ap_editions_categories_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `edition_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ap_editions_categories` (`edition_id`),
  KEY `idx_ap_editions_categories_0` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_icons`
--

CREATE TABLE IF NOT EXISTS `ap_icons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon_1` varchar(100) DEFAULT NULL,
  `icon_2` varchar(100) DEFAULT NULL,
  `icon_3` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `ap_icons`
--

INSERT INTO `ap_icons` (`id`, `icon_1`, `icon_2`, `icon_3`) VALUES
(1, 'D:\\xampp\\htdocs\\yii\\newsapps\\protected\\..\\images\\icon\\iconnews_1.jpg', NULL, NULL),
(2, '/yii/newsapps//images/icon/iconnews_2.jpg', NULL, NULL),
(3, 'http://static.freepik.com/free-photo/lorem-ipsum_21338031.jpg', NULL, NULL),
(4, 'http://tinyurl.com/m7j3zlg', 'http://tinyurl.com/m7j3zlg', 'http://tinyurl.com/m7j3zlg'),
(5, '/yii/newsapps//images/icon/iconnews_5.jpg', NULL, NULL),
(6, 'http://localhost/yii/newsapps/images/icon/iconnews_6.jpg', NULL, NULL),
(7, 'http://localhost/yii/newsapps/images/icon/iconnews_7.jpg', 'http://localhost/yii/newsapps/images/icon/iconnews_7.jpg', 'http://localhost/yii/newsapps/images/icon/iconnews_7.jpg'),
(8, 'http://localhost/yii/newsapps/images/icon/iconnews_8.jpg', 'http://localhost/yii/newsapps/images/icon/iconnews_8.jpg', 'http://localhost/yii/newsapps/images/icon/iconnews_8.jpg'),
(9, 'http://tinyurl.com/ohh8wjl', 'http://tinyurl.com/ohh8wjl', 'http://tinyurl.com/ohh8wjl'),
(10, 'http://tinyurl.com/m38eoob', 'http://tinyurl.com/m38eoob', 'http://tinyurl.com/m38eoob'),
(11, 'http://tinyurl.com/o2q6xhv', 'http://tinyurl.com/o2q6xhv', 'http://tinyurl.com/o2q6xhv'),
(12, 'http://tinyurl.com/jw3nzdu', 'http://tinyurl.com/jw3nzdu', 'http://tinyurl.com/jw3nzdu'),
(13, 'http://tinyurl.com/mjy2lub', 'http://tinyurl.com/mjy2lub', 'http://tinyurl.com/mjy2lub'),
(14, 'http://tinyurl.com/l8p84bt', 'http://tinyurl.com/l8p84bt', 'http://tinyurl.com/l8p84bt'),
(15, 'http://tinyurl.com/pvrn3ch', 'http://tinyurl.com/pvrn3ch', 'http://tinyurl.com/pvrn3ch'),
(16, 'http://tinyurl.com/qy8fh6y', 'http://tinyurl.com/qy8fh6y', 'http://tinyurl.com/qy8fh6y'),
(17, 'http://tinyurl.com/pk7a5ng', 'http://tinyurl.com/pk7a5ng', 'http://tinyurl.com/pk7a5ng'),
(18, '/yii/newsapps//images/icon/News 2.jpg', '/yii/newsapps//images/icon/News 2.jpg', '/yii/newsapps//images/icon/News 2.jpg'),
(19, 'www.ayam.com', 'www.ayam.com', 'www.ayam.com'),
(20, 'asdas', 'asdas', 'asdas'),
(21, 'http://tinyurl.com/pfne7mx', 'http://tinyurl.com/pfne7mx', 'http://tinyurl.com/pfne7mx'),
(22, 'http://tinyurl.com/nmymcl5', 'http://tinyurl.com/nmymcl5', 'http://tinyurl.com/nmymcl5'),
(23, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ap_icons_bak`
--

CREATE TABLE IF NOT EXISTS `ap_icons_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon_1` varchar(100) DEFAULT NULL,
  `icon_2` varchar(100) DEFAULT NULL,
  `icon_3` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_item_news`
--

CREATE TABLE IF NOT EXISTS `ap_item_news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) DEFAULT NULL,
  `id_icon` int(11) DEFAULT NULL,
  `news_title` varchar(100) DEFAULT NULL,
  `published_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `count_view` int(11) DEFAULT '0',
  `count_comment` int(11) DEFAULT '0',
  `id_url` bigint(20) DEFAULT NULL,
  `news_tags` varchar(200) DEFAULT NULL,
  `active_flag` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` varchar(30) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nid` int(11) DEFAULT NULL,
  `content_desc` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `id_comment` varchar(100) DEFAULT NULL,
  `comment_flag` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ap_item_news` (`id_category`),
  KEY `idx_ap_item_news_0` (`id_url`),
  KEY `idx_ap_item_news_1` (`id_icon`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `ap_item_news`
--

INSERT INTO `ap_item_news` (`id`, `id_category`, `id_icon`, `news_title`, `published_date`, `count_view`, `count_comment`, `id_url`, `news_tags`, `active_flag`, `created_by`, `created_date`, `nid`, `content_desc`, `id_comment`, `comment_flag`) VALUES
(2, 1, 2, 'Lorem Ipsum v2', '2014-07-14 17:00:00', 0, 0, 2, NULL, 1, 'Veronica', '2014-07-15 08:08:39', NULL, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure d', '0', 0),
(4, 1, 4, 'Zozo ganteng', '2014-07-14 17:00:00', 0, 0, 4, NULL, 1, 'vero', '2014-07-15 08:25:10', NULL, 'KYAAAAAA', '0', 0),
(5, 1, 5, 'How to make novel', '2014-07-14 17:00:00', 0, 0, 5, NULL, 1, 'Veronica Mutiana', '2014-07-15 09:47:16', NULL, 'Introductions are a great way to set the reader up for your novels. The main uses for introductions, formally known as prologues, are to: 1. Give readers the general idea of the story 2. Get the reader interested And... 3. To help yourself get the fe', '0', 0),
(6, 1, 6, 'Ayam', '2014-07-15 17:00:00', 0, 0, 6, NULL, 0, 'Ver', '2014-07-16 04:45:09', NULL, 'Ayam Goreng enak sekali', '0', 0),
(9, 1, 9, 'dari url jadi upload', '2014-08-13 17:00:00', 0, 0, 9, NULL, 1, 'aada', '2014-07-16 09:17:02', NULL, 'ini dari url', '0', 0),
(10, 1, 10, 'ayam terbang', '2014-07-15 17:00:00', 0, 0, 10, NULL, 0, 'vero', '2014-07-16 09:39:18', NULL, 'test', '0', 0),
(11, 1, 11, 'windows', '2014-07-15 17:00:00', 0, 0, 11, NULL, 1, 'windows', '2014-07-16 09:52:47', NULL, 'tentang windows', '0', 0),
(12, 2, 12, 'ayam v2', '2014-07-16 17:00:00', 0, 0, 12, NULL, 1, 'vero 2', '2014-07-17 04:45:34', NULL, 'ayam v2', '0', 0),
(13, 2, 13, 'ayam ke 3', '2014-07-16 17:00:00', 0, 0, 13, NULL, 0, 'ayam terbang', '2014-07-17 04:46:54', NULL, 'tragedi ayam ke 3', '0', 0),
(14, 1, 15, 'wkwkwkk', '2014-07-16 17:00:00', 0, 0, 15, NULL, 0, 'wkwkwk', '2014-07-17 07:43:57', NULL, 'wwlwkwkwk', '0', 0),
(15, 1, 16, 'Lorem Ipsum v3', '2014-09-25 17:00:00', 0, 0, 16, NULL, 1, 'Googlee', '2014-07-17 07:46:20', NULL, 'Lorem ipsum', '0', 0),
(16, 1, 17, 'How to make novel prolog', '2014-07-16 17:00:00', 0, 0, 17, NULL, 1, 'Veronica', '2014-07-17 08:39:36', NULL, 'There are 6 step to make novel prolog', '0', 0),
(17, 1, 21, 'a', '2014-08-04 17:00:00', 0, 0, 18, NULL, 0, 'ver', '2014-08-05 05:59:35', NULL, 'a', '0', 0),
(18, 1, 22, 'b', '2014-08-04 17:00:00', 0, 0, 19, NULL, 1, 'ver', '2014-08-05 06:09:33', NULL, 'b', '0', 0),
(19, 1, 23, 'Zozo ganteng', '2011-03-06 17:00:00', 0, 0, 20, NULL, 1, 'Zozo Si Ganteng', '2014-08-06 10:57:19', NULL, 'Kenapa sih? Zozo ganteng banget!!!', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ap_item_news_bak`
--

CREATE TABLE IF NOT EXISTS `ap_item_news_bak` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) DEFAULT NULL,
  `id_icon` int(11) DEFAULT NULL,
  `news_title` varchar(100) DEFAULT NULL,
  `published_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `count_view` int(11) DEFAULT NULL,
  `count_comment` int(11) DEFAULT NULL,
  `id_url` bigint(20) DEFAULT NULL,
  `news_tags` varchar(200) DEFAULT NULL,
  `active_flag` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` varchar(20) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ap_item_news` (`id_category`),
  KEY `idx_ap_item_news_0` (`id_url`),
  KEY `idx_ap_item_news_1` (`id_icon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_news_detail`
--

CREATE TABLE IF NOT EXISTS `ap_news_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_detail` text NOT NULL,
  `content_img` text NOT NULL,
  `content_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `ap_news_detail`
--

INSERT INTO `ap_news_detail` (`id`, `content_detail`, `content_img`, `content_id`) VALUES
(2, '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '', 2),
(4, '<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	1</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Know when to write a prologue.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;Not all prologues have to be written at the immediate start of writing your story. If you aren&#39;t sure what to do with this, then ignore it until you get the general feel of the plot. After getting the feel, or even after you&#39;ve finished it, you can always go back and write a prologue.</span></p>\r\n<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	2</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Know what sort of story to put a prologue with.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;Prologues are sometimes not appropriate for certain stories. It all depends on the first paragraph of your first chapter. Write the first paragraph. Does it sound like you&#39;re going on with the story too quickly? If that is the case, then your story probably does need a prologue.</span></p>\r\n<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	3</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Choose which characters should be featured in a prologue.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;Perhaps writing about characters that will only be briefly mentioned in the story is your way to go. Prologues often do not include the main characters or any sidekicks the protagonist might have. They can feature the antagonists, minor villains, the guardian, allies of the heroes, or practically any other character. Sometimes, prologues don&#39;t even have to feature a character! They can illustrate a dramatic event vital to the story, such as a disaster that might have triggered the grand adventure for the heroes.</span></p>\r\n<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	4</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Consider language.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;Language is one of the keys to getting a reader interested. If you are writing about an event, then read about a catastrophic disaster in a history book. Using that example, write your prologue like you were writing a history book. If you are creating a general scene starring, let&#39;s say, the antagonist (in this case, a rich banker) strutting around his castle, telling his cronies how he is going to take over the world, then act natural. Write like you were writing any other part of the story.</span></p>\r\n<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	5</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Consider length.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;The length of your prologue can vary. It can be ten pages long, if you like, or it can just be a page or two. If you are writing about a scene, then it will probably be longer then it would be if it was an event.</span></p>\r\n<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	6</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Be consistent in tone.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;Tone is how the narrator tells the story.</span></p>\r\n', '', 5),
(5, '<p>\r\n	Ini tentang ayam goreng</p>\r\n', '', 6),
(8, '<p>\r\n	Dummy data</p>\r\n', '', 10),
(9, '<p>\r\n	test daari url ke upload</p>\r\n', '', 9),
(10, '<p>\r\n	wkwkwkk</p>\r\n', '', 14),
(11, '<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	1</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Know when to write a prologue.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;Not all prologues have to be written at the immediate start of writing your story. If you aren&#39;t sure what to do with this, then ignore it until you get the general feel of the plot. After getting the feel, or even after you&#39;ve finished it, you can always go back and write a prologue.</span></p>\r\n<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	2</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Know what sort of story to put a prologue with.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;Prologues are sometimes not appropriate for certain stories. It all depends on the first paragraph of your first chapter. Write the first paragraph. Does it sound like you&#39;re going on with the story too quickly? If that is the case, then your story probably does need a prologue.</span></p>\r\n<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	3</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Choose which characters should be featured in a prologue.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;Perhaps writing about characters that will only be briefly mentioned in the story is your way to go. Prologues often do not include the main characters or any sidekicks the protagonist might have. They can feature the antagonists, minor villains, the guardian, allies of the heroes, or practically any other character. Sometimes, prologues don&#39;t even have to feature a character! They can illustrate a dramatic event vital to the story, such as a disaster that might have triggered the grand adventure for the heroes.</span></p>\r\n<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	4</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Consider language.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;Language is one of the keys to getting a reader interested. If you are writing about an event, then read about a catastrophic disaster in a history book. Using that example, write your prologue like you were writing a history book. If you are creating a general scene starring, let&#39;s say, the antagonist (in this case, a rich banker) strutting around his castle, telling his cronies how he is going to take over the world, then act natural. Write like you were writing any other part of the story.</span></p>\r\n<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	5</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Consider length.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;The length of your prologue can vary. It can be ten pages long, if you like, or it can just be a page or two. If you are writing about a scene, then it will probably be longer then it would be if it was an event.</span></p>\r\n<div class="step_num" style="margin: 0px; padding: 8px 15px 3px 0px; font-size: 43px; color: rgb(84, 84, 84); float: left; font-weight: bold; font-family: Helvetica, arial, sans-serif; line-height: 25px;">\r\n	6</div>\r\n<p>\r\n	<b class="whb" style="margin: 0px; padding: 0px; color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px;">Be consistent in tone.</b><span style="color: rgb(84, 84, 84); font-family: Helvetica, arial, sans-serif; font-size: 16px; line-height: 25px; background-color: rgb(255, 255, 255);">&nbsp;Tone is how the narrator tells the story.</span></p>\r\n', '', 16),
(12, '<p>\r\n	aaa</p>\r\n', '', 17),
(13, '<p>\r\n	b</p>\r\n', '', 18),
(14, '<p>\r\n	AAAAAAAAA</p>\r\n', '', 19);

-- --------------------------------------------------------

--
-- Table structure for table `ap_tags`
--

CREATE TABLE IF NOT EXISTS `ap_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(50) DEFAULT NULL,
  `tag_desc` varchar(255) DEFAULT NULL,
  `tag_meta` varchar(100) DEFAULT NULL,
  `tid` int(11) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ap_tags` (`tag_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_tags_bak`
--

CREATE TABLE IF NOT EXISTS `ap_tags_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(50) DEFAULT NULL,
  `tag_meta` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ap_tags` (`tag_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_template`
--

CREATE TABLE IF NOT EXISTS `ap_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `themes_id` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `themes_id` (`themes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ap_template`
--

INSERT INTO `ap_template` (`id`, `themes_id`, `created`) VALUES
(1, 1, '2014-07-24 07:24:09'),
(2, 2, '2014-07-24 07:24:11'),
(3, 3, '2014-07-24 07:39:39'),
(4, 4, '2014-08-06 03:57:25'),
(5, 6, '2014-08-06 03:57:25'),
(6, 7, '2014-08-06 03:57:25'),
(7, 8, '2014-08-06 03:57:25'),
(8, 9, '2014-08-06 03:57:25'),
(9, 10, '2014-08-06 03:57:44');

-- --------------------------------------------------------

--
-- Table structure for table `ap_urls`
--

CREATE TABLE IF NOT EXISTS `ap_urls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url_1` varchar(200) DEFAULT NULL,
  `url_2` varchar(200) DEFAULT NULL,
  `url_3` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `ap_urls`
--

INSERT INTO `ap_urls` (`id`, `url_1`, `url_2`, `url_3`) VALUES
(1, '/yii/newsapps//index.php/?r=site/view/1', NULL, NULL),
(2, '/yii/newsapps//index.php/?r=site/view/2', NULL, NULL),
(3, 'http://websitetips.com/articles/copy/lorem/ipsum.txt', NULL, NULL),
(4, 'http://tinyurl.com/nmhkdsn', 'http://tinyurl.com/nmhkdsn', 'http://tinyurl.com/nmhkdsn'),
(5, '/yii/newsapps//index.php/?r=site/view/4', NULL, NULL),
(6, '/yii/newsapps/index.php/?r=site/view&id=5', NULL, NULL),
(7, '/yii/newsapps/index.php/?r=site/view&id=6', '/yii/newsapps/index.php/?r=site/view&id=6', '/yii/newsapps/index.php/?r=site/view&id=6'),
(8, '/yii/newsapps/index.php/?r=site/view&id=7', '/yii/newsapps/index.php/?r=site/view&id=7', '/yii/newsapps/index.php/?r=site/view&id=7'),
(9, 'http://tinyurl.com/ouuc2eq', 'http://tinyurl.com/ouuc2eq', 'http://tinyurl.com/ouuc2eq'),
(10, 'http://tinyurl.com/kdhb4va', 'http://tinyurl.com/kdhb4va', 'http://tinyurl.com/kdhb4va'),
(11, 'http://tinyurl.com/qyvhawl', 'http://tinyurl.com/qyvhawl', 'http://tinyurl.com/qyvhawl'),
(12, 'Error', 'Error', 'Error'),
(13, 'Error', 'Error', 'Error'),
(14, 'http://tinyurl.com/py2ss6g', 'http://tinyurl.com/py2ss6g', 'http://tinyurl.com/py2ss6g'),
(15, 'http://tinyurl.com/py2ss6g', 'http://tinyurl.com/py2ss6g', 'http://tinyurl.com/py2ss6g'),
(16, 'http://tinyurl.com/odpkt9z', 'http://tinyurl.com/odpkt9z', 'http://tinyurl.com/odpkt9z'),
(17, 'http://tinyurl.com/p8ad4yd', 'http://tinyurl.com/p8ad4yd', 'http://tinyurl.com/p8ad4yd'),
(18, 'http://tinyurl.com/pd4z473', 'http://tinyurl.com/pd4z473', 'http://tinyurl.com/pd4z473'),
(19, 'http://tinyurl.com/ng49x7f', 'http://tinyurl.com/ng49x7f', 'http://tinyurl.com/ng49x7f'),
(20, 'http://tinyurl.com/pj6e68s', 'http://tinyurl.com/pj6e68s', 'http://tinyurl.com/pj6e68s');

-- --------------------------------------------------------

--
-- Table structure for table `ap_urls_bak`
--

CREATE TABLE IF NOT EXISTS `ap_urls_bak` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url_1` varchar(200) DEFAULT NULL,
  `url_2` varchar(200) DEFAULT NULL,
  `url_3` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_user_gcm`
--

CREATE TABLE IF NOT EXISTS `ap_user_gcm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regid` varchar(250) NOT NULL,
  `userid` int(11) NOT NULL,
  `comment_uid` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `username` varchar(100) NOT NULL,
  `comment_pass` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_migration`
--

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profiles`
--

CREATE TABLE IF NOT EXISTS `tbl_profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profiles_fields`
--

CREATE TABLE IF NOT EXISTS `tbl_profiles_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `field_type` varchar(50) NOT NULL DEFAULT '',
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` text,
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` text,
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_themes`
--

CREATE TABLE IF NOT EXISTS `tbl_themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `color` varchar(10) DEFAULT NULL,
  `icon_pack` varchar(250) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `license` int(11) DEFAULT '0',
  `price` int(11) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_themes`
--

INSERT INTO `tbl_themes` (`id`, `name`, `color`, `icon_pack`, `active`, `license`, `price`, `created`) VALUES
(1, 'Black', '#000000', NULL, 1, 0, 0, '2014-07-24 07:23:59'),
(2, 'Yellow', '#ffff00', NULL, 1, 0, 0, '2014-07-24 07:24:02'),
(3, 'DarkBlue', '#0A246A', NULL, 1, 0, 0, '2014-07-24 07:24:02'),
(4, 'White', 'white', NULL, 0, 0, 0, '2014-08-06 03:54:02'),
(6, 'Basic', '#ebebeb', NULL, 0, 0, 0, '2014-08-06 03:54:49'),
(7, 'Pink', 'pink', NULL, 0, 0, 0, '2014-08-06 03:55:32'),
(8, 'Wheat', 'wheat', NULL, 0, 0, 0, '2014-08-06 03:55:47'),
(9, 'Blue', 'Blue', NULL, 0, 0, 0, '2014-08-06 03:56:11'),
(10, 'Red', 'Red', NULL, 0, 0, 0, '2014-08-06 03:56:24'),
(11, 'Green', 'green', NULL, 0, 0, 0, '2014-08-06 03:58:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username` (`username`),
  UNIQUE KEY `user_email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `email`, `activkey`, `superuser`, `status`, `create_at`, `lastvisit_at`) VALUES
(1, 'admin', 'e3274be5c857fb42ab72d786e281b4b8', 'awmobile.d@gmail.com', 'dc7453aac821e83f545142028b67ee88', 1, 1, '2013-06-26 07:39:48', '2014-08-08 04:38:00'),
(2, 'awmobile', '85807fa75dd6d9c46276eb34324bcb1f', 'terry@teltics.com', '3b136375cee2ed7d9cf2117f7b35bd5c', 1, 1, '2013-07-01 02:34:19', '2013-07-02 06:53:05'),
(6, 'vero', 'cc491de401e5dbcde41ef91090975f42', 'vershin93@gmail.com', '0', 0, 1, '2014-07-23 10:15:18', '2014-08-05 04:04:46');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ap_apps_icon`
--
ALTER TABLE `ap_apps_icon`
  ADD CONSTRAINT `ap_apps_icon_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `ap_template` (`id`);

--
-- Constraints for table `ap_template`
--
ALTER TABLE `ap_template`
  ADD CONSTRAINT `ap_template_ibfk_1` FOREIGN KEY (`themes_id`) REFERENCES `tbl_themes` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
