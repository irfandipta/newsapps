<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $username;
	public $password;
        public $status;
        public $superuser;
	public $rememberMe;

	private $_identity;
        private $conn;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}
        
        public function __construct() {
            $this->conn = Yii::app()->db;
        }

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>'Remember me next time',
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			if(!$this->_identity->authenticate())
				$this->addError('password','Incorrect username or password.');
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
                        $query = $this->conn->createCommand("SELECT status,superuser FROM tbl_users WHERE username='".$this->username."' LIMIT 1");
                        $result = $query->queryAll();
                        Yii::app()->user->superuser = $result[0]["superuser"];
                        Yii::app()->user->status = $result[0]['status'];
                        $this->changeLastVisit();
			return true;
		}
		else
			return false;
	}
        
        protected function changeLastVisit(){
        $query = $this->conn->createCommand("UPDATE tbl_users SET lastvisit_at = now() WHERE username=:name");
        $param = array(":name"=>Yii::app()->user->name);
        $query->execute($param);
    }
        
        public function flagStatus(){
            $query = $this->conn->createCommand("SELECT status FROM tbl_users WHERE username='".$this->username."'");
            $status = $query->queryScalar();
            return $status;
        }
}
