<?php
class Report extends CFormModel
{
	public $conn;
	
	public function __construct(){
        $this->conn = Yii::app()->db;
	}
	
	public function listAppUser($startDate, $endDate){
		$query = $this->conn->createcommand('SELECT date(created) AS created, COUNT(date(created)) AS count FROM ap_user_gcm WHERE date(created) >= "'.
		           $startDate.'" AND date(created) <= "'.$endDate.'" GROUP BY date(created);');
		$result = $query->queryAll();
		return $result;
	}
	
	public function listAppUserByOS($os, $startDate, $endDate){
		$query = $this->conn->createCommand('SELECT date(created) AS created, COUNT(date(created)) AS count FROM ap_user_gcm WHERE platform="'.$os.'" 
					AND date(created) >= "'.$startDate.'" AND date(created) <= "'.$endDate.'"GROUP BY date(created);');
		$result = $query->queryAll();
		return $result;
	}
	
	public function listInstallByOS($os, $startDate, $endDate){
		$query = $this->conn->createCommand('SELECT COUNT(date(created)) AS count FROM ap_user_gcm WHERE platform = "'.$os.'"
             		AND date(created) >= "'.$startDate.'" AND date(created) <= "'.$endDate.'";');
		$result = $query->queryAll();
		return $result;
	}
}
?>