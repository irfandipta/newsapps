<?php

class IndexForm extends CFormModel
{
	private $_identity;
        public $conn;
        
     public function __construct() {
        $this->conn = Yii::app()->db;
     }

	public function getUserAndroid()
	{
         $command = $this->conn->createCommand("SELECT id from ap_user_gcm where platform='android';");
         $result = $command->queryAll();
         return $result;
	}
	
	public function getUserIos()
	{
         $command = $this->conn->createCommand("SELECT id from ap_user_gcm where platform='ios';");
         $result = $command->queryAll();
         return $result;
	}
	
	public function getNotif()
	{
		 $command = $this->conn->createCommand('SELECT * from ap_notification order by id desc limit 1;');
         $result = $command->queryAll();
         return $result;
	}
	
	public function getContentTitle($id)
	{
		$command = $this->conn->createCommand("SELECT news_title from ap_item_news where id = ".$id.";");
		$result = $command->queryAll();
		return $result;
	}
}
