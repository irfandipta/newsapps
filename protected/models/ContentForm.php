<?php
class ContentForm extends CFormModel
{
	public $conn;
        public $categories;
	public $title;
	public $description;
	public $contentDetail;
        public $url;
	public $icon;
        public $iconUrl;
        public $publishedDate;
        public $createdBy;
        public $activeFlag;
        public $imagePath;
        public $flagFiture; //untuk kasih tau pke fitur apa. 1=upload, 2=url
        public $flagFiture2;
        
        public $searchKeyword;
        
        private $headerID;
        private $urlID;
        private $iconID;
	
	public function __construct(){
            $this->conn = Yii::app()->db;
	}

	public function rules()
	{
            return array(
		array('categories, title, description, createdBy, activeFlag, publishedDate', 'required'),
                //array('contentDetail, url, iconUrl', 'required'),
		array('icon', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true,),
            );
	}	
        
        public function setDetail($post){
            $this->contentDetail = $post['contentDetail'];
            $this->url = $post['url'];
            $this->iconUrl = $post['iconUrl'];
        }
        
        public function createNewContent($url, $icon){
            $this->urlID = $this->getNewestID("ap_urls") + 1; //dapetin id terbaru dari table ap_urls
            $this->iconID = $this->getNewestID("ap_icons") + 1;//dapetin id terbaru dari table ap_icons
            
            $rowUrl = $this->insertUrl($url); //masukkan ke table ap_urls
            $rowIcon = $this->insertIcon($icon); //masukkan ke table ap_icons
            
            if($rowIcon > 0 && $rowUrl > 0){                
                $this->headerID = $this->getNewestID("ap_item_news") + 1; //dapetin id terakhir dari table ap_item_news
                //masukkan ke table ap_item_news
                $query = $this->conn->createCommand('INSERT INTO ap_item_news (id, id_category, id_icon, news_title, published_date,'
                        . 'count_view, count_comment, id_url, news_tags, active_flag, created_by, created_date, content_desc,'
                        . 'id_comment, comment_flag) VALUES (:id, :category, :icon, :title, :publish, 0, 0, :url, null, :flag, :author,'
                        . 'now(), :desc, 0, 0)');
                $parameter = array(':id'=>$this->headerID,':category'=>$this->categories, ':icon'=>$this->iconID, ':title'=>$this->title, 
                    ':publish'=>$this->publishedDate, ':url'=>$this->urlID,  ':flag'=>$this->activeFlag, 'author'=>$this->createdBy,
                    ':desc'=>$this->description);
                $rowNews = $query->execute($parameter);
                
                if($this->flagFiture == 2) return true; //kalo fitur URL ga usah tambah detail
                
                if($rowNews > 0 ){   
                    $rowDetail = $this->insertContentDetail(); //masukkan ke table ap_news_detail
                    if($rowDetail > 0)
                        return true;
                    else
                        return false;
                }
                else return false;
            }
            else return false;
        }
	
	public function getCategoryList(){
            $query = $this->conn->createcommand('SELECT category_name, id FROM ap_categories');
            $list = $query->queryAll();
            return $list;
        }
        
        public function getNewestID($table){
            $query = $this->conn->createcommand('SELECT max(id) FROM '.$table);
            $id = $query->queryScalar();
            if($id == null) $id = 0;
            return $id;
        }
        
        public function countAllItemContent(){
            $query = $this->conn->createcommand('SELECT count(*) FROM ap_item_news');
            $count = $query->queryScalar();
            return $count;
        }
        
        public function getItemData($page,$pageSize){
            $query = $this->conn->createCommand('SELECT ap_item_news.id as id, news_title, published_date, active_flag, '
                    . 'created_by, content_desc, icon_1 FROM ap_item_news, ap_icons WHERE ap_icons.id=id_icon ORDER BY created_date DESC '
                    . 'LIMIT '.(($page-1)*$pageSize).','.$pageSize);
            $result = $query->queryAll();
            return $result;
        }
        
        public function setItemNews($id){
            $query = $this->conn->createCommand('SELECT id_category, news_title, content_desc,'
                    . ' id_icon, icon_1, id_url, url_1, published_date, created_by, active_flag FROM ap_item_news, '
                    . 'ap_icons, ap_urls WHERE id_icon=ap_icons.id AND id_url=ap_urls.id AND ap_item_news.id='.$id);
            $result = $query->queryAll();
            
            $query = $this->conn->createCommand('SELECT id, content_detail FROM ap_news_detail WHERE content_id='.$id);
            $detail = $query->queryAll();
            
            $this->headerID = $id;
            $this->iconID = $result[0]['id_icon'];
            $this->urlID = $result[0]['id_url'];
            $this->categories = $result[0]['id_category'];
            $this->title = $result[0]['news_title'];
            $this->description = $result[0]['content_desc'];
            $this->createdBy = $result[0]['created_by'];
            $this->publishedDate = $result[0]['published_date'];
            $this->activeFlag = $result[0]['active_flag'];
            $this->imagePath = $result[0]['icon_1'];
            
            $lastDetail = count($detail)-1;
            if(count($detail) > 0){ //sebelumnya menggunakan fitur upload
                $this->contentDetail = $detail[$lastDetail]['content_detail'];
                $this->flagFiture = 1;
            }
            else{ //sebelumnya menngunakan fitur URL
                $this->iconUrl = $result[0]['icon_1'];
                $this->url = $result[0]['url_1'];
                $this->flagFiture = 2;
            }
        }
        
        public function deleteContent($id){
            $query = $this->conn->createCommand('DELETE FROM ap_news_detail WHERE content_id = :id');
            $parameter = array(':id'=>$id);
            $query->execute($parameter);
            $query2 = $this->conn->createCommand('DELETE FROM ap_item_news WHERE id = :id');
            $query2->execute($parameter);
        }
        
        public function updateContent(){            
            $query = $this->conn->createCommand('UPDATE ap_item_news SET id_category=:cat, news_title=:title, '
                    . 'content_desc=:desc, published_date=:date, created_by=:author, active_flag=:flag WHERE id=:id');
            $parameter = array(':cat'=>$this->categories, ':title'=>$this->title, ':desc'=>$this->description, 
                ':date'=>$this->publishedDate, ':author'=>$this->createdBy, ':flag'=>$this->activeFlag, ':id'=>$this->headerID);
            $update = $query->execute($parameter);
            if($update > 0) {return true;}
            else {return false;}
        }
        
        public function updateContentDetail(){
            $query = $this->conn->createCommand('UPDATE ap_news_detail SET content_detail=:content WHERE content_id=:id');
            $parameter = array(':content'=>$this->contentDetail, ':id'=>$this->headerID);
            $update = $query->execute($parameter);
            if($update > 0) {return true;}
            else {return false;}
        }
        
        public function insertContentDetail(){
            $query = $this->conn->createCommand('INSERT INTO ap_news_detail (content_detail, content_id) VALUES (:detail, :id)');
            $parameter = array(':detail'=>$this->contentDetail, ':id'=>$this->headerID);
            $insert = $query->execute($parameter);
            if($insert > 0) {return true;}
            else {return false;}
        }
        
         public function updateUrl($url){
            $query = $this->conn->createCommand('UPDATE ap_urls SET url_1=:url, url_2=:url, url_3=:url WHERE id=:id');
            $parameter = array(':url'=>$url, ':id'=>$this->urlID);
            $update = $query->execute($parameter);
            if($update > 0) {return true;}
            else {return false;}
        }
        
        public function insertUrl($url){
            $query = $this->conn->createCommand('INSERT INTO ap_urls (id, url_1, url_2, url_3) VALUES (:id, :url, :url, :url)');
            $parameter = array(':url'=>$url, ':id'=>$this->urlID);
            $insert = $query->execute($parameter);
            if($insert > 0) {return true;}
            else {return false;}
        }
        
        public function updateIcon($icon){
            $query = $this->conn->createCommand('UPDATE ap_icons SET icon_1=:icon, icon_2=:icon, icon_3=:icon WHERE id=:id');
            $parameter = array(':icon'=>$icon, ':id'=>$this->iconID);
            $update = $query->execute($parameter);
            if($update > 0) {return true;}
            else {return false;}
        }
        
        public function insertIcon($icon){
            $query = $this->conn->createCommand('INSERT INTO ap_icons (id, icon_1, icon_2, icon_3) VALUES (:id, :icon, :icon, :icon)');
            $parameter = array(':icon'=>$icon, ':id'=>$this->iconID);
            $insert = $query->execute($parameter);
            if($insert > 0) {return true;}
            else {return false;}
        }
        
        public function getIconID(){
            return $this->iconID;
        }
        
        public function getUrlID(){
            return $this->urlID;
        }
        
        public function getHeaderID(){
            return $this->headerID;
        }
        
        public function setFlagFiture($post){
            $this->flagFiture = $post;
        }
        
        public function setFlagFiture2($post){
            $this->flagFiture2 = $post;
        }
  
        
        public function setSearchKeyword($post){
            $this->searchKeyword = $post;
        }
        
        public function searchResult($page,$pageSize){
            $query = $this->conn->createCommand('SELECT ap_item_news.id as id, news_title, published_date, active_flag, '
                    . 'created_by, content_desc, icon_1 FROM ap_item_news, ap_icons WHERE ap_icons.id=id_icon AND '
                    . '(news_title LIKE "%'.urlDecode($this->searchKeyword).'%" OR content_desc LIKE "%'.$this->searchKeyword.'%") ORDER BY created_date DESC '
                    . 'LIMIT '.(($page-1)*$pageSize).','.$pageSize);
            $result = $query->queryAll();
            return $result;
        }
        
        public function countSearchResult(){
            $query = $this->conn->createCommand('SELECT count(ap_item_news.id) FROM ap_item_news, ap_icons WHERE ap_icons.id=id_icon AND '
                    . '(news_title LIKE "%'.urlDecode($this->searchKeyword).'%" OR content_desc LIKE "%'.$this->searchKeyword.'%")');
            $count = $query->queryScalar();
            return $count;
        }
}
