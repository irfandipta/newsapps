<?php
class APIContent extends CFormModel
{
    private $conn;
    
    public function __construct(){
        $this->conn = Yii::app()->db;
    }
    
    public function latestContent(){
        $query = $this->conn->createCommand('SELECT a.id, id_category, category_name, a.id_icon, icon_1, news_title, published_date, count_view, '
                . 'count_comment, id_url, url_1, news_tags, a.active_flag, a.created_by, a.created_date, nid, content_desc, id_comment, comment_flag '
                . 'FROM ap_item_news a, ap_urls, ap_icons, ap_categories c WHERE id_url = ap_urls.id AND a.id_icon = ap_icons.id AND id_category = c.id ORDER BY a.id DESC;');
        $result = $query->queryAll();
        return $result;
    }
    
    public function contentByCategory($id){
        $query = $this->conn->createCommand('SELECT a.id, id_category, category_name, a.id_icon, icon_1, news_title, published_date, count_view, '
                . 'count_comment, id_url, url_1, news_tags, a.active_flag, a.created_by, a.created_date, nid, content_desc, id_comment, comment_flag '
                . 'FROM ap_item_news a, ap_urls, ap_icons, ap_categories c WHERE id_url = ap_urls.id AND a.id_icon = ap_icons.id AND id_category = c.id AND 
				(id_category = '.$id.' OR parentID = '.$id.') ORDER BY a.id DESC;');

        $result = $query->queryAll();
        return $result;
    }
    
    public function contentByContentID($id){
        $query = $this->conn->createCommand('SELECT a.id, id_category, category_name, a.id_icon, icon_1, news_title, published_date, 
            count_view, count_comment, id_url, url_1, news_tags, a.active_flag, a.created_by, a.created_date, nid, content_desc, id_comment, 
            comment_flag FROM ap_item_news a, ap_urls, ap_icons, ap_categories c WHERE id_url = ap_urls.id AND a.id_icon = ap_icons.id AND 
            id_category = c.id AND a.id = '.$id.' ORDER BY a.id DESC;');
        $result = $query->queryAll();
        return $result;
    }
	
	public function contentSearchAll($key){
		$query = $this->conn->createCommand('SELECT a.id, id_category, category_name, a.id_icon, icon_1, news_title, published_date, 
				count_view, count_comment, id_url, url_1, news_tags, a.active_flag, a.created_by, a.created_date, nid, content_desc, id_comment,
				comment_flag FROM ap_item_news a, ap_urls, ap_icons, ap_categories c WHERE id_url = ap_urls.id AND a.id_icon = ap_icons.id AND 
				id_category = c.id AND (news_title LIKE "%'.$key.'%" OR content_desc LIKE "%'.$key.'%") ORDER BY a.id DESC;');
        $result = $query->queryAll();
        return $result;
	}
    
    public function contentSearchByCat($key, $cat, $dateStart, $dateEnd){
        $query = $this->conn->createCommand('SELECT a.id, id_category, category_name, a.id_icon, icon_1, news_title, published_date, count_view, 
            count_comment, id_url, url_1, news_tags, a.active_flag, a.created_by, a.created_date, nid, content_desc, id_comment, comment_flag 
            FROM ap_item_news a, ap_urls, ap_icons, ap_categories c WHERE id_url = ap_urls.id AND a.id_icon = ap_icons.id AND id_category = c.id AND 
            ((news_title LIKE "%'.$key.'%" OR content_desc LIKE "%'.$key.'%") AND (id_category='.$cat.' OR parentID='.$cat.') AND '
                . 'a.created_date>="'.$dateStart.' 00:00:00" AND a.created_date<="'.$dateEnd.' 23:59:59") ORDER BY a.id DESC;');
        $result = $query->queryAll();
        return $result;
    }
	
	public function contentSearchAllCat($key, $dateStart, $dateEnd){
		$query = $this->conn->createCommand('SELECT a.id, id_category, category_name, a.id_icon, icon_1, news_title, published_date, count_view, 
            count_comment, id_url, url_1, news_tags, a.active_flag, a.created_by, a.created_date, nid, content_desc, id_comment, comment_flag 
            FROM ap_item_news a, ap_urls, ap_icons, ap_categories c WHERE id_url = ap_urls.id AND a.id_icon = ap_icons.id AND id_category = c.id AND 
            ((news_title LIKE "%'.$key.'%" OR content_desc LIKE "%'.$key.'%") AND a.created_date>="'.$dateStart.' 00:00:00" AND 
			a.created_date<="'.$dateEnd.' 23:59:59") ORDER BY a.id DESC;');
        $result = $query->queryAll();
        return $result;
	}
    
    public function countView($contentID){
        $query = $this->conn->createCommand('SELECT count_view FROM ap_item_news WHERE id='.$contentID);
        $count = $query->queryScalar();
        $query2 = $this->conn->createCommand('UPDATE ap_item_news SET count_view=:count WHERE id=:ID');
        $param2 = array(':ID'=>$contentID, ':count'=>($count+1));
        $update = $query2->execute($param2);
        if($update > 0){
            $count+=1;
        }
        return $count;
    }
}