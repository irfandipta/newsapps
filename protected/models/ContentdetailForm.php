<?php
class ContentdetailForm extends CFormModel
{
    public $conn;
    public $headerID;
    
    public function __construct(){
        $this->conn = Yii::app()->db;
    }
    
    public function getNewsDetail($id){
        $query = $this->conn->createCommand("SELECT * FROM ap_news_detail WHERE id =". $id);
        $result = $query->queryAll();
        if(count($result) != 0)
            $this->headerID = $result[0]['content_id'];
        else
            $this->headerID = 0;
        return $result;
    }
    
    public function getTitle(){
        $query = $this->conn->createCommand("SELECT news_title FROM ap_item_news WHERE id =". $this->headerID);
        $result = $query->queryScalar();
        return $result;
    }
    
    public function getImagePath(){
        $result = null;
        
        if($this->headerID != 0){
            $query = $this->conn->createCommand("SELECT id_icon FROM ap_item_news WHERE id =". $this->headerID);
            $id = $query->queryScalar();
            $query = $this->conn->createCommand("SELECT icon_1 FROM ap_icons WHERE id =". $id);
            $result = $query->queryScalar();
        }
        
        return $result;
    }
}