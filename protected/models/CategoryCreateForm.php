<?php

class CategoryCreateForm extends CFormModel
{
	public $categoryName;
	public $createdBy;
	public $color;
	public $active;
	public $pic;
	public $picUrl;
	public $connection;
	public $imageShow;
	public $parentId;
	
	public function rules()
	{
		return array(
			array('categoryName, createdBy, active', 'required'),
			array('pic','file','types'=>'jpg, gif, png', 'allowEmpty'=>false),
		);
	}
	
	public function __construct()
	{
		parent::__construct();
		$this->connection = Yii::app()->db;
	}
	
	public function exist($categoryName){
		$command = $this->connection->createCommand("select * from ap_categories where category_name='" . $categoryName . "'");
		$item = $command->queryAll();
		if(count($item) == 1) return true;
		else return false;
	}
	
	public function getNewId()
	{
		$command = $this->connection->createCommand("select * from ap_icons");
		$result = $command->queryAll();
		if(count($result) == 0) return 1;
		else
		{
			$command2 = $this->connection->createCommand("select MAX(id) from ap_icons");
			$result2 = $command2->queryAll();
			$id = $result2[0]['MAX(id)']+1;
			return $id;
		}
	}
	
	public function getCategoryDetail($id)
	{
		$command = $this->connection->createCommand("select a.id, a.sort_code, a.category_name, a.active_flag, a.created_by, a.color_code, a.parentId, b.icon_1 from ap_categories a, ap_icons b where a.id_icon = b.id and a.id = ".$id.";");
		$result= $command->queryAll();
		
		$this->categoryName = $result[0]['category_name'];
		$this->createdBy = $result[0]['created_by'];
		$this->picUrl = $result[0]['icon_1'];
		$this->color = $result[0]['color_code'];
		$this->active = $result[0]['active_flag'];
	}
	
	public function getCategoryWithParent()
	{
		$command = $this->connection->createCommand("select id,category_name from ap_categories where parentId = 0");
		$result = $command->queryAll();
		return $result;
	}
	
	public function getCategoryList()
	{
		$command = $this->connection->createCommand("select a.id, a.sort_code, a.category_name, a.active_flag, a.created_by, a.color_code, a.parentId, b.icon_1 from ap_categories a, ap_icons b where a.id_icon = b.id and a.parentId = 0 order by a.sort_code");
		$result = $command->queryAll();
		return $result;
	}
	
	public function getSubCategoryList()
	{
		$command = $this->connection->createCommand("select a.id, a.sort_code, a.category_name, a.active_flag, a.created_by, a.color_code, a.parentId, b.icon_1 from ap_categories a, ap_icons b where a.id_icon = b.id and a.parentId != 0 order by a.sort_code");
		$result = $command->queryAll();
		return $result;
	}
	
	public function add($categoryName, $pic, $createdBy, $color, $active, $parentId)
	{
		$NowSC;
	
		$command1 = $this->connection->createCommand("insert into ap_icons(icon_1,icon_2,icon_3) values(:img,:img,:img)");
		$parameters1 = array(":img"=>$pic);
		$command1->execute($parameters1);
		
		$command2 = $this->connection->createCommand("select max(id) from ap_icons;");
		$icon = $command2->queryAll();
		$id_icon = $icon[0]['max(id)'];
		
		$command31 = $this->connection->createCommand("select * from ap_categories");
		$result31 = $command31->queryAll();
		if(count($result31) == 0) $NowSC = 1;
		else
		{
			$command32 = $this->connection->createCommand("select MAX(sort_code) from ap_categories");
			$result32 = $command32->queryAll();
			$NowSC = $result32[0]['MAX(sort_code)']+1;
		}
		
		$command3 = $this->connection->createCOmmand("insert into ap_categories (id_icon, sort_code, category_name, active_flag, created_by, color_code, parentId) values(:id_icon, :sort_code, :category_name, :active_flag, :created_by, :color_code, :parentId)");
		$parameters3 = array(":id_icon"=>$id_icon, ":sort_code"=>$NowSC, ":category_name"=>$categoryName, ":active_flag"=>$active, ":created_by"=>$createdBy, ":color_code"=>$color, ":parentId"=>$parentId);
		$command3->execute($parameters3);
		
		$command4 = $this->connection->createCOmmand("select id from ap_categories where category_name = '".$categoryName."';");
		$result4 = $command4->queryAll();
		$nowID = $result4[0]['id'];

		$command5 = $this->connection->createCOmmand("update ap_categories set tid = ".$nowID." where id = ".$nowID.";");
		$command5->execute();
	}
	
	public function delete($id)
	{
		$command = $this->connection->createCommand("select id from ap_categories where id = '".$id."';");
		$result = $command->queryAll();
		$parentId = $result[0]['id'];

		$command2 = $this->connection->createCommand("Delete from ap_categories where parentId = '".$parentId."';");
		$command2->execute();
		
		$command3 = $this->connection->createCommand("Delete from ap_categories where id = '".$id."';");
		$command3->execute();
	}
	
	public function SwapSortCode($id1, $sort1, $id2, $sort2)
	{
		$command1 = $this->connection->createCommand("Update ap_categories set sort_code = ".$sort2." where id = ".$id1.";");
		$command1->execute();
		$command2 = $this->connection->createCommand("Update ap_categories set sort_code = ".$sort1." where id = ".$id2.";");
		$command2->execute();
	}
	
	public function update($id, $categoryName, $createdBy, $color, $active, $image)
	{
		echo $id.", ".$categoryName.", ".$createdBy.", ".$color.", ".$active.", ".$image;
		$command1 = $this->connection->createCommand("Select id_icon from ap_categories where id = ".$id.";");
		$result1 = $command1->queryAll();
		$id_icon = $result1[0]['id_icon'];
		
		$command2 = $this->connection->createCommand("Update ap_icons set icon_1 = '".$image."', icon_2 = '".$image."', icon_3 = '".$image."' where id = ".$id_icon.";");
		$command2->execute();
		
		$command3 = $this->connection->createCommand("Update ap_categories set category_name = '".$categoryName."', created_by = '".$createdBy."', color_code = '".$color."', active_flag = ".$active." where id = ".$id.";");
		$command3->execute();
	}
}

?>