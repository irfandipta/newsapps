<?php

class BroadcastForm extends CFormModel
{
	private $_identity;
        public $conn;

	public function rules()
	{
		return array(
			// username and password are required
			array('username, password', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}
        
        public function __construct() {
            $this->conn = Yii::app()->db;
        }

	public function getUser()
	{
            $command = $this->conn->createCommand('SELECT * from ap_user_gcm;');
            $result = $command->queryAll();
            return $result;
	}
	
	public function insertNotif($reg_id,$platform,$contentId)
	{
		
         $command = $this->conn->createCommand("INSERT INTO ap_notification (reg_id, platform, content_id) VALUES (".$reg_id.",'".$platform."',".$contentId.");");
         $result = $command->execute();
	}
}
