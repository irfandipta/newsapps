<?php
class Reminder extends CFormModel
{
	public $conn;
	public $userID;
	public $toID;
	public $message;
	
	public function __construct(){
        $this->conn = Yii::app()->db;
		$queryID = $this->conn->createcommand("SELECT id FROM tbl_users WHERE username='".Yii::app()->user->name."';");
		$this->userID = $queryID->queryScalar();
	}
	
	public function setData($toID, $message){
		$this->toID = $toID;
		$this->message = $message;
	}
	
	public function getListUser(){
		$query = $this->conn->createcommand('SELECT id, username, superuser FROM tbl_users WHERE id != '.$this->userID.' ORDER BY id;');
		$result = $query->queryAll();
		return $result;
	}
	
	public function insertReminder(){
		$query = $this->conn->createcommand('INSERT INTO tbl_reminder(`from`,`to`,message) VALUES(:from,:to,:msg);');
		$param = array(':from'=>$this->userID, ':to'=>$this->toID, ':msg'=>$this->message);
		$insert = $query->execute($param);
		if($insert > 0) {return true;}
		else {return false;}
	}
	
	public function getListReminder(){
		$query = $this->conn->createcommand('SELECT r.id, `from`, (SELECT username FROM tbl_users a WHERE a.id=r.`from`) AS fromName, 
					`to`, (SELECT username FROM tbl_users b WHERE b.id=r.`to`) AS toName, message, parent_id, done, `read`, created 
					FROM tbl_reminder r WHERE (`to`='.$this->userID.' OR `from`='.$this->userID.') AND parent_id=0 ORDER BY done ASC, r.created DESC;');
		$result = $query->queryAll();
		return $result;
	}
	
	public function getReminderComment($parentID){
		$query = $this->conn->createcommand('SELECT r.id, `from`, (SELECT username FROM tbl_users a WHERE a.id=r.`from`) AS fromName, 
					`to`, (SELECT username FROM tbl_users b WHERE b.id=r.`to`) AS toName, message, done, `read`, created FROM tbl_reminder r 
					WHERE (`to`='.$this->userID.' OR `from`='.$this->userID.') AND parent_id='.$parentID.' ORDER BY r.created ASC;');
		$result = $query->queryAll();
		return $result;
	}
	
	public function getDashboardReminder(){
		$query = $this->conn->createcommand('SELECT r.id, `from`, (SELECT username FROM tbl_users a WHERE a.id=r.`from`) AS fromName, 
					`to`, (SELECT username FROM tbl_users b WHERE b.id=r.`to`) AS toName, message, parent_id, done, `read`, created FROM tbl_reminder r 
					WHERE (`to`='.$this->userID.' OR `from`='.$this->userID.') AND r.`done`=0 AND r.parent_id=0 ORDER BY r.created DESC;');
		$result = $query->queryAll();
		return $result;
	}
	
	public function deleteReminder($id){
		$query = $this->conn->createcommand('DELETE FROM tbl_reminder WHERE id=:id OR parent_id=:id');
		$param = array(':id'=>$id);
		$delete = $query->execute($param);
		if($delete > 0)	{return true;}
		else {return false;}
	}
	
	public function addComment($parentID, $to, $message){
		$query = $this->conn->createcommand('INSERT INTO tbl_reminder(`from`,`to`,message,parent_id) VALUES(:from,:to,:message,:parent)');
		$param = array(':from'=>$this->userID, ':to'=>$to, ':message'=>$message, ':parent'=>$parentID);
		$insert = $query->execute($param);
		if($insert > 0) {return true;}
		else {return false;}		
	}
	
	public function setDone($done, $id){
		$query = $this->conn->createcommand('UPDATE tbl_reminder SET `done`=:done WHERE id=:id OR parent_id=:id');
		$param = array(':id'=>$id, ':done'=>$done);
		$update = $query->execute($param);
		if($update > 0)	{return true;}
		else {return false;}
	}
	
	public function setRead($id){
		$query = $this->conn->createcommand('UPDATE tbl_reminder SET `read`=1 WHERE id=:id OR parent_id=:id');
		$param = array(':id'=>$id);
		$update = $query->execute($param);
		if($update > 0)	{return true;}
		else {return false;}	
	}
}
?>