<?php

/* 
 * Author : Veronica Mutiana
 */

class ThemeForm extends CFormModel
{
    public $templateID;
    public $icon;
    public $prevIcon;
	public $splashScreen;
	public $prevSplashScreen;
	public $appName;
	public $appShortName;
    
    private $conn;
    private $jmlIcon;
    
    public function rules() {
        return array(
            array('icon, splashScreen', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true,),
			array('appName, appShortName', 'required'),
         );
    }
    
    public function __construct() {
        $this->conn = Yii::app()->db;
        $query = $this->conn->createCommand("SELECT count(*) FROM ap_apps_icon");
        $this->jmlIcon = $query->queryScalar();
        if($this->jmlIcon > 0){
            $query = $this->conn->createCommand("SELECT template_id, logo, splash_screen_img, app_name, app_short_name FROM ap_apps_icon LIMIT 1");
            $result = $query->queryAll();
            $this->templateID = $result[0]['template_id'];
            $this->prevIcon = $result[0]['logo'];
			$this->prevSplashScreen = $result[0]['splash_screen_img'];
			$this->appName = $result[0]['app_name'];
			$this->appShortName = $result[0]['app_short_name'];
        }
        else{
            $this->templateID = 0;
        }
    }
    
    public function getAllThemes(){
        $query = $this->conn->createCommand("SELECT a.id as id, themes_id, name, color FROM ap_template a, tbl_themes b WHERE themes_id=b.id");
        $result = $query->queryAll();
        return $result;
    }
    
    public function updateAppsIcon($icon, $splash){
        $query = "";
        if($this->jmlIcon == 0){
            $query = $this->conn->createCommand("INSERT INTO ap_apps_icon(template_id, logo, updated, app_name, app_short_name, splash_screen_img) 
						VALUES (:tID, :icon, now(), :name, :sname, :ssimg)");
        }
        else{
            $query = $this->conn->createCommand("UPDATE ap_apps_icon SET template_id=:tID, logo=:icon, updated=now(), app_name=:name, app_short_name=:sname,
						splash_screen_img=:ssimg");
        }
        $param = array(':tID'=>$this->templateID, ':icon'=>$icon, ':name'=>$this->appName, ':sname'=>$this->appShortName, ':ssimg'=>$splash);
        $query->execute($param);
    }
}