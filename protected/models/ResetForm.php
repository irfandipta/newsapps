<?php

class ResetForm extends CFormModel
{
	public $email;
	public $connection;
	public $id;
	public $username;
	public $password;
	public $passwordconf;
	
	public function rules()
	{
		return array(
			// username and password are required
			array('email, password, passwordconf', 'required'),
		);
	}
	
	public function __construct()
	{
		parent::__construct();
		$this->connection = Yii::app()->db;
	}
	
	public function checkEmail()
	{
		$command = $this->connection->createCommand("SELECT id, username, password from tbl_users where email = '".$this->email."';");
		$result = $command->queryAll();
		$this->id = $result[0]['id'];
		$this->username = $result[0]['username'];
		$this->password = $result[0]['password'];
		
		if(count($result)==1)
		{
			$activationLink = Yii::app()->request->hostInfo.Yii::app()->user->returnUrl."?r=site/ConfReset&id=".md5($this->id);
			
			$subject = "Reset Email Confirmation";
			
			$bodyText = "Your account is:<br/>Username: ".$this->username."<br/><br/>". "To reset your password, click this link.<br/><a href='".$activationLink."' target='_blank'>".$activationLink."</a>.";
			$to = $this->email;
			$this->sendEmail($subject, $bodyText, $to);
                        Yii::app()->user->setFlash('successFlash','Reset password confirmation has been sent to your e-mail, check your inbox or spam folder');
		}
		else
		{
                    Yii::app()->user->setFlash('errorFlash','Data not valid. Username, password, and email Required, or email format not valid.');
                }
	}
	
	public function getData($id)
	{
		$command = $this->connection->createCommand("SELECT id, username from tbl_users HAVING md5(id) = '".$id."';");
		$result = $command->queryAll();
		$this->id = $id;
		$this->username = $result[0]['username'];
	}
	
	public function updatePass($id)
	{
		$command = $this->connection->createCommand("Update tbl_users set password ='".md5($this->password)."' where md5(id) = '".$id."';");
		$command->execute();
	}
	
	protected function sendEmail($subject, $bodyText, $to){
            $message = new YiiMailMessage;
            $message->subject = $subject;
            $message->setBody($bodyText, 'text/html');                
            $message->addTo($to);
            $message->from = "do.notreply@teltics.com";   
            Yii::app()->mail->send($message);
        }
}