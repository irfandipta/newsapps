<?php

/* 
 * Author : Veronica Mutiana
 */

class UserForm extends CFormModel
{
    private $conn;
    public $id;
    public $username;
    public $password;
    public $confirmPassword;
    public $newPassword;
    public $confNewPassword;
    public $email;
    public $status;
    public $superuser;
    public $passFlag;
    
    public function __construct() {
        $this->conn = Yii::app()->db;
    }
    
    public function rules() {
        return array(
            array('username, password, confirmPassword, email, status, superuser', 'required'),
        );
    }	
    
    public function userExist(){
        $query = $this->conn->createCommand("SELECT COUNT(*) FROM tbl_users WHERE username = '".$this->username."' OR email = '".$this->email."'");
        $exist = $query->queryScalar();
        if($exist > 0) {return true;}
        else {return false;}
    }
    
    public function createUser(){
        $query = $this->conn->createCommand("INSERT INTO tbl_users (username, password, email, activkey, superuser, status, create_at) "
                . "VALUE (:uname, :pass, :email, 0, 0, 0, now())");
        $param = array(':uname'=>$this->username, ':pass'=>md5($this->password), ':email'=>$this->email);
        $create = $query->execute($param);
        $query = $this->conn->createCommand("SELECT id FROM tbl_users WHERE username='".$this->username."' LIMIT 1");
        $id = $query->queryScalar();
        $this->id = $id;
        if($create > 0) {return true;}
        else {return false;}
    }
    
    public function changeStatus($idHash){
        $query = $this->conn->createCommand("SELECT id FROM tbl_users HAVING md5(id)='".$idHash."'");
        $id = $query->queryScalar();
        if($id == null) return 3; //id not found
        $query = $this->conn->createCommand("UPDATE tbl_users SET status = 1 WHERE id = :id");
        $param = array(':id'=>$id);
        $change = $query->execute($param);
        if($change > 0) return 1;   //berhasil di aktivasi
        else return 2;  //account sudah aktif
    }
    
    public function getUserList(){
        $query = $this->conn->createCommand("SELECT * FROM tbl_users");
        $result = $query->queryAll();
        return $result;
    }
    
    public function getProfileByName($name){
        $query = $this->conn->createCommand("SELECT * FROM tbl_users WHERE username ='".$name."'");
        $profile = $query->queryAll();
        return $profile;
    }
    
    public function getProfileById($id){
        $query = $this->conn->createCommand("SELECT * FROM tbl_users WHERE id =".$id);
        $profile = $query->queryAll();
        return $profile;
    }
    
    public function setProfile($id){
        $profile = $this->getProfileById($id);
        $this->id = $id;
        $this->username = $profile[0]['username'];
        $this->email = $profile[0]['email'];
        $this->superuser = $profile[0]['superuser'];
        $this->status = $profile[0]['status'];
        $this->password = $profile[0]['password'];
        $this->confirmPassword = $this->password;
    }
    
    public function updateAccount($id){
        $query = $this->conn->createCommand("UPDATE tbl_users SET username=:name, password=:password, email=:email, superuser=:s, status=:stat WHERE id=:id");
        $param = array(':name'=>$this->username, ':password'=>$this->password, ':email'=>$this->email, ':s'=>$this->superuser, ':stat'=>$this->status, ':id'=>$id);
        $query->execute($param);
    }
    
    public function deleteAccount($id){
        $query = $this->conn->createCommand("DELETE FROM tbl_users WHERE id=:id");
        $param = array(':id'=>$id);
        $query->execute($param);
    }
}