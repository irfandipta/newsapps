<?php

class BuyIconForm extends CFormModel
{
	public $connection;
	
	public function rules()
	{
		return array(
			array('categoryName, createdBy, active', 'required'),
			array('pic','file','types'=>'jpg, gif, png', 'allowEmpty'=>false),
		);
	}
	
	public function __construct()
	{
		parent::__construct();
		$this->connection = Yii::app()->db;
	}
	
	public function getList()
	{
		$command = $this->connection->createCommand("SELECT * from tbl_themes");
		$result = $command->queryAll();
		return $result;
	}
}

?>