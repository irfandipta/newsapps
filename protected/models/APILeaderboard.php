<?php
/* 
 * Author : Veronica Mutiana
 */
class APILeaderboard extends CFormModel
{
    private $conn;
    
    public function __construct(){
        $this->conn = Yii::app()->db;
    }
    
    public function updateData($regID, $distance, $time){
        $msg = 'Success';
        $query = $this->conn->createCommand('SELECT id FROM ap_user_gcm WHERE regid="'.$regID.'" LIMIT 1');
        $user_id = $query->queryScalar(); //id dari regid 
        if($user_id != null){
            $query = $this->conn->createCommand('SELECT count(*) FROM tbl_leaderboard WHERE user_id = '.$user_id.' LIMIT 1');
            $countLeadership = $query->queryScalar();
            $speed = round(($distance/$time), 2); 
            if($countLeadership != 0){ //id di leadership ada => update
                $query = $this->conn->createCommand('UPDATE tbl_leaderboard SET distance=:distance, time=:time, speed=:speed, updated=now() WHERE user_id=:id');
                $param = array(':id'=>$user_id, ':distance'=>$distance, ':time'=>$time, ':speed'=>$speed);
                $query->execute($param);
            }
            else { //id tidak ada => insert
                $query = $this->conn->createCommand('INSERT INTO tbl_leaderboard (user_id, distance, time, speed, updated) VALUES (:id, :distance, :time, :speed, now())');
                $param = array(':id'=>$user_id, ':distance'=>$distance, ':time'=>$time, ':speed'=>$speed);
                $query->execute($param);
            }
        }
        else{
            $msg = 'regID not found'; //regid tidak ada
        }
        return $msg;
    }
    
    public function listRanking(){
        $query = $this->conn->createCommand('SELECT @curRank := @curRank + 1 AS rank, user_id, distance, time, speed, updated, username '
                . 'FROM tbl_leaderboard a, ap_user_gcm b, (SELECT @curRank := 0) r WHERE b.id=user_id ORDER BY a.speed DESC');
        $result = $query->queryAll();
        return $result;
    }
    
    public function changeUserName($regid, $uname){
        $query = $this->conn->createCommand('UPDATE ap_user_gcm SET username=:uname WHERE regid=:id');
        $param = array(':uname'=>$uname, ':id'=>$regid);
        $query->execute($param);
    }
}