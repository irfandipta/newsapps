<?php

class APICategoryForm extends CFormModel
{
	public $connection;
	
	public function __construct()
	{
		parent::__construct();
		$this->connection = Yii::app()->db;
	}
	
	public function getCategoryList()
	{
		$command = $this->connection->createCommand("select a.*, b.icon_1 from ap_categories a, ap_icons b where a.id_icon = b.id and a.parentID = 0 order by a.sort_code");
		$result = $command->queryAll();
		return $result;
	}
	
	public function getSubCategoryList($parentId)
	{
		$command = $this->connection->createCommand('select a.*, b.icon_1 from ap_categories a, ap_icons b where a.id_icon = b.id and a.parentID = "'.$parentId.'" order by a.sort_code');
		$result = $command->queryAll();
		return $result;
	}
}