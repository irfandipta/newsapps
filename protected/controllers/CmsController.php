<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class CmsController extends Controller
{
    public $URLMANIC = array('base' => 'http://54.179.163.240:8899/',  
                                 'android' => array('appsname' => 'JakThon' , 'env' => 'P'), 
                                 'ios' => array('appsname' => 'JakThonIOS' , 'env' => 'S')
                                );
        
	public function init(){	//init jika belum melakukan login
		if(Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->user->returnUrl.'?r=site/login');
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('contentcreate','update','admin','delete','contentList','broadcast'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
	public function actionCategoryCreate()
	{
		$model = new CategoryCreateForm;
		$categoryList = $model->getCategoryWithParent();
		$path = NULL;
		$errorMsg = NULL;
		
		if(isset($_POST['CategoryCreateForm']))
		{			
			$model->attributes = $_POST['CategoryCreateForm'];
			$model->color = $_POST['CategoryCreateForm']['color'];
			$model->pic=CUploadedFile::getInstance($model,'pic');
			$model->active = $_POST['CategoryCreateForm']['active'];
			$model->picUrl = $_POST['CategoryCreateForm']['picUrl'];
			$model->parentId = $_POST['CategoryCreateForm']['parentId'];
			
			if($model->categoryName == NULL || $model->createdBy == NULL || $model->active == NULL || ($model->pic == NULL && $model->picUrl == NULL))
			{
				if($model->categoryName == null) $errorMsg .= "Please fill the category name! <br/>";
				if($model->createdBy == null) $errorMsg .= "Please fill the Author (Created By)! <br/>";
				if($model->active == null) $errorMsg .= "Please check the active! <br/>";
				if($model->pic == NULL && $model->picUrl == NULL) $errorMsg .= "Please upload an image or fill the URL! <br/>";
				Yii::app()->user->setFlash('errorFlash',$errorMsg);
				$this->refresh();
			}
			else
			{
				if(!$model->exist($model->categoryName))
				{			
					if($model->pic != NULL || $model->picUrl != NULL)
						if($model->pic != NULL)
						{
							$images_path = YiiBase::getPathOfAlias('webroot').'/images';
							$ext = $model->pic->getExtensionName();
							$fileName = "category_".strtolower(str_replace(' ', '_', $model->categoryName)).".".$ext;	
							$path = $images_path ."/" . $fileName;
							$model->pic->saveAs($path,true);
							$path = Yii::app()->request->hostInfo.Yii::app()->baseUrl."/images/".$fileName;
							$model->add($model->categoryName, $this->shortenURL($path), $model->createdBy, $model->color, $model->active, $model->parentId);
						}
						else
						{
							$model->add($model->categoryName, $this->shortenURL($model->picUrl), $model->createdBy, $model->color, $model->active, $model->parentId);
						}
					
					Yii::app()->user->setFlash('successFlash','Category add succeed');
					$this->refresh();
				}
				else
				{
					Yii::app()->user->setFlash('errorFlash','Category name existed, please change category name!');
					$this->refresh();
				}
			}
		}
		$this->render('CategoryCreate',array('model'=>$model,'categoryList'=>$categoryList));
	}
	
	public function actionCategoryList()
	{
		$model = new CategoryCreateForm;
		$data = $model->getCategoryList();
		$subdata = $model->getSubCategoryList();
		$this->render('CategoryList',array('data'=>$data, 'subdata'=>$subdata));
	}
	
	public function actionCategoryDelete()
	{
		$id = Yii::app()->getRequest()->getParam('id');
		$model = new CategoryCreateForm;
		$model->delete($id);
		
		$data = $model->getCategoryList();
		$subdata = $model->getSubCategoryList();
		$this->render('CategoryList',array('data'=>$data, 'subdata'=>$subdata));
	}
	
	public function actionCategoryEdit()
	{
		$id = Yii::app()->getRequest()->getParam('id');
		$model = new CategoryCreateForm;
		$model->getCategoryDetail($id);
		$oldname = $model->categoryName;
		$flag = 1;
		
		if(isset($_POST['CategoryCreateForm']))
		{
			$model->attributes = $_POST['CategoryCreateForm'];
			$model->color = $_POST['CategoryCreateForm']['color'];
			$model->pic=CUploadedFile::getInstance($model,'pic');
			$model->active = $_POST['CategoryCreateForm']['active'];
			$model->picUrl = $_POST['CategoryCreateForm']['picUrl'];

			if($model->categoryName != $oldname)
			{
				if($model->exist($model->categoryName))
				{
					$flag = 0;
				}
			}
			
			if($flag==1)
			{
				if($model->pic==null)
				$model->update($id, $model->categoryName, $model->createdBy, $model->color, $model->active, $model->picUrl);
				else
				{
					$images_path = YiiBase::getPathOfAlias('webroot').'/images';
					$ext = $model->pic->getExtensionName();
					$fileName = "category_".strtolower(str_replace(' ', '_', $model->categoryName)).".".$ext;	
                                        $path = $images_path ."/" . $fileName;
					$model->pic->saveAs($path,true);
					$path = Yii::app()->request->hostInfo.Yii::app()->baseUrl."/images/".$fileName;
					$model->update($id, $model->categoryName, $model->createdBy, $model->color, $model->active, $this->shortenURL($path));
				}
				$this->refresh();
			}
		}
		$this->render('CategoryEdit',array('model'=>$model));
	}
	
	public function actionSwapSortCode()
	{
		$id1 = Yii::app()->getRequest()->getParam('id1');
		$sort1 = Yii::app()->getRequest()->getParam('sort1');
		$id2 = Yii::app()->getRequest()->getParam('id2');
		$sort2 = Yii::app()->getRequest()->getParam('sort2');
		
		$model = new CategoryCreateForm;
		$model->SwapSortCode($id1,$sort1,$id2,$sort2);
		
		$data = $model->getCategoryList();
		$subdata = $model->getSubCategoryList();
		$this->render('CategoryList',array('data'=>$data, 'subdata'=>$subdata));
	}
        
        public function actionContentCreate(){
            $model = new ContentForm;
            $catList = $model->getCategoryList();
            if(isset($_POST['ContentForm'])) {
                $model->setDetail($_POST['ContentForm']);
                $model->attributes = $_POST['ContentForm'];
                $model->icon = CUploadedFile::getInstance($model,'icon');
                if($model->title == null || $model->description == null || $model->createdBy == null ||
                    ($model->contentDetail == null && $model->url == null)){
                        $errorMsg = "Error:<br/><ul>";
                        if($model->title == null) $errorMsg .= "<li>Title can't empty.</li>";
                        if($model->description == null) $errorMsg .= "<li>Description can't empty.</li>";
                        if($model->createdBy == null) $errorMsg .= "<li>Author (Created By) can't empty.</li>";
                        if($model->contentDetail == null && $model->url == null) $errorMsg .= "<li>Choose either Upload or URL and then fill it.</li>";
                        ///if($model->contentDetail != null && $model->url != null) $errorMsg .= "<li>You can't use two fiture at once. Choose either Upload or URL and then empty the other.</li>";
                        $errorMsg .= "</ul>";
                        Yii::app()->user->setFlash('errorFlash',$errorMsg);
                        $this->refresh();
                }
                else{
                   $model->setFlagFiture($_POST['ContentForm']['flagFiture']);
                   if($model->validate()){
                       $iconn = "";
                       $urll = "";
                       if($model->flagFiture == 1){ //fiture upload
                            if($model->icon != null){ //  upload icon
                                $bspath = YiiBase::getPathOfAlias('webroot').'/images';
                                $ext = $model->icon->getExtensionName(); //ambil extension gambar
                                $filename = "iconnews_".($model->getNewestID("ap_icons")+1).".".$ext; //namafile : iconnews_id.jpg
                                $path = $bspath."/".$filename;
                                $model->icon->saveAs($path,true);
                                $iconn = $this->shortenURL(Yii::app()->request->hostInfo.Yii::app()->baseUrl."/images/".$filename);
                            }
                            $urll = $this->shortenURL(Yii::app()->request->hostInfo.Yii::app()->user->returnUrl."/?r=site/view&id=".($model->getNewestID("ap_news_detail")+1));
                       }
                       else if($model->flagFiture == 2){ //fiture URL
                           if($model->iconUrl != null){
                               $iconn = $this->shortenURL($model->iconUrl);
                           }
                           $urll = $this->shortenURL($model->url);
                       }
                        $create = $model->createNewContent($urll, $iconn);
                        if($create){
                            Yii::app()->user->setFlash('successFlash','Success to add new Content');
                            $this->refresh();
                        }
                        else{
                            Yii::app()->user->setFlash('errorFlash','Failed to add new Content');
                            $this->refresh();
                        }
                    }
                }
            }
            $this->render('contentCreate', array('model'=>$model, 'catList'=>$catList));
        }
        
        public function actionContentList(){
            $model = new ContentForm;
            $page = (isset($_GET['page']) ? $_GET['page'] : 1); 
            $pageSize = 5;
            $data = null;
            if(isset($_GET['keyword'])){
                $model->setSearchKeyword(urlEncode($_GET['keyword']));
                $data = $model->searchResult($page,$pageSize);
                $count = $model->countSearchResult();
            }
            else{
                $data = $model->getItemData($page,$pageSize);
                $count = $model->countAllItemContent();
            }
            //pagination
            $pages=new CPagination($count);
            $pages->setPageSize($pageSize);
            //$pages->applyLimit($criteria);
    
            $url['ajax_broadcast'] = Yii::app()->request->hostInfo.Yii::app()->baseUrl.'/?r=cms/broadcast';
            $this->render('contentList', array('model'=>$model, 'data'=>$data, 'pages'=>$pages, 'item_count'=>$count, 
                'page_size'=>$pageSize, 'url' => $url));
        }
        
        public function actionEditContent(){
            $model = new ContentForm;
            $newsID = Yii::app()->getRequest()->getParam('id');
            $catList = $model->getCategoryList();
            $model->setItemNews($newsID);
            if(isset($_POST['ContentForm'])) {
                $post = $_POST['ContentForm'];
                if($post['title'] == null || $post['description'] == null || $post['createdBy'] == null){ //validation null value
                        $errorMsg = "Error:<br/><ul>";
                        if($post['title'] == null) $errorMsg .= "<li>Title can't empty.</li>";
                        if($post['description'] == null) $errorMsg .= "<li>Description can't empty.</li>";
                        if($post['createdBy'] == null) $errorMsg .= "<li>Author (Created By) can't empty.</li>";$errorMsg .= "</ul>";
                        Yii::app()->user->setFlash('errorFlash',$errorMsg);
                        $this->refresh();
                }
                else{
                    $success = false;
                    $model->attributes = $_POST['ContentForm'];
                    $model->setDetail($_POST['ContentForm']);
                    $model->setFlagFiture2($post['flagFiture2']);
                    $model->icon = CUploadedFile::getInstance($model,'icon');
                    if($model->validate()){
                        if($model->flagFiture == 1 && $model->flagFiture2 == 1){ //sebelum: upload, submit: upload
                            if($model->contentDetail != null){
                                $f1 = $model->updateContent();
                                $f2 = $model->updateContentDetail();
                                if($model->icon != null){ //upload gambar
                                    $bspath = YiiBase::getPathOfAlias('webroot').'/images';
                                    $ext = $model->icon->getExtensionName(); //ambil extension gambar
                                    $filename = "iconnews_".$model->getIconID().".".$ext; //namafile : iconnews_id.jpg
                                    $path = $bspath."/".$filename;
                                    $model->icon->saveAs($path,true);
                                    $iconn = $this->shortenURL(Yii::app()->request->hostInfo.Yii::app()->baseUrl."/images/".$filename);
                                    $f3 = $model->updateIcon($iconn);
                                }
                                $success = true;
                            }
                        }
                        else if($model->flagFiture == 2 && $model->flagFiture2 == 2){ //sebelum: url, submit: url
                            if($model->url != null){
                                $f1 = $model->updateContent();
                                $f2 = $model->updateUrl($this->shortenURL($model->url));
                                if($model->iconUrl != null) {$f3 = $model->updateIcon($this->shortenURL($model->iconUrl));}
                                else {$f3 = $model->updateIcon(null);}
                                $success = true;
                            }
                        }
                        else if($model->flagFiture == 1 && $model->flagFiture2 == 2){ //sebelum: upload, submit: url
                            if($model->url != null){
                                $f1 = $model->updateContent();
                                if($model->iconUrl != null) {$f2 = $model->updateIcon($this->shortenURL($model->iconUrl));}
                                else {$f2 = $model->updateIcon(null);}
                                $f3 = $model->updateUrl($this->shortenURL($model->url));
                                $success = true;
                            }
                        }
                        else if($model->flagFiture == 2 && $model->flagFiture2 == 1){ //sebelum: url, submit: upload
                            $iconn = "";
                            if($model->contentDetail != null){
                                $f1 = $model->updateContent();
                                $urll = $this->shortenURL(Yii::app()->request->hostInfo.Yii::app()->user->returnUrl."index.php/?r=site/view&id=".($model->getHeaderID()));
                                $f2 = $model->updateUrl($urll);
                                if($model->icon != null){ //upload gambar
                                    $bspath = YiiBase::getPathOfAlias('webroot').'/images';
                                    $ext = $model->icon->getExtensionName(); //ambil extension gambar
                                    $filename = "iconnews_".$model->getIconID().".".$ext; //namafile : iconnews_id.jpg
                                    $path = $bspath."/".$filename;
                                    $model->icon->saveAs($path,true);
                                    $iconn = $this->shortenURL(Yii::app()->request->hostInfo.Yii::app()->baseUrl."/images/".$filename);
                                }
                                $f3 = $model->updateIcon($iconn);
                                $f4 = $model->insertContentDetail();
                                $success = true;
                            }
                        }
                        if($success){
                            Yii::app()->user->setFlash('successFlash','Success to Update Content');
                            $this->refresh();
                        }
                        else{
                            Yii::app()->user->setFlash('successFlash','There is no change from previous data.');
                            $this->refresh();
                        }
                    }
                }
            }
            $this->render('contentEdit', array('catList'=>$catList, 'model'=>$model));
        }
        
        public function actionDeleteContent(){
            $model = new ContentForm;
            $newsID = Yii::app()->getRequest()->getParam('id');
            if($newsID != null) $model->deleteContent($newsID);
            $this->redirect(Yii::app()->user->returnUrl.'index.php/?r=cms/contentList');
        }
        
        public function actionUserCreate(){
            $model = new UserForm;
            if(isset($_POST['UserForm'])){
                $model->attributes = $_POST['UserForm'];
                $model->status = 0;
                $model->superuser = 0;
                if($model->username == null || $model->password == null || $model->confirmPassword == null || $model->email == null || 
                        ($model->password != $model->confirmPassword)) { //validation
                    $errorMsg = "Error:<br/><ul>";
                        if($model->username == null) $errorMsg .= "<li>Username can't empty.</li>";
                        if($model->password == null) $errorMsg .= "<li>Password can't empty.</li>";
                        if($model->email == null) $errorMsg .= "<li>Email can't empty.</li>";
                        if($model->password != $model->confirmPassword) $errorMsg .= "<li>Password confirmation is wrong</li>";
                        $errorMsg .= "</ul>";
                        Yii::app()->user->setFlash('errorFlash',$errorMsg);
                        $this->refresh();
                }
                else{
                    if($model->validate()){ //validasi form
                        if(!$model->userExist()){   //username dan email belum ada
                            $create = $model->createUser(); 
                            if($create == true){    //user berhasil dibuat
                                //send email activation
                                $activationLink = Yii::app()->request->hostInfo.Yii::app()->user->returnUrl."?r=site/userActivation&id=".md5($model->id);
                                $subject = "Activate Your NewsApps Account";
                                $bodyText = "Your account is:<br/>Username: ".$model->username."<br/>Password: ".$model->password."<br/><br/>"
                                        . "To activate this account, click this link.<br/><a href='".$activationLink."' target='_blank'>".$activationLink."</a>.";
                                $to = $model->email;
                                $this->sendEmail($subject, $bodyText, $to);
                                Yii::app()->user->setFlash('successFlash','Create user success. Please check your email to activate your account.');
                                $this->refresh();
                            }
                        }
                        else{   //username sudah ada
                            Yii::app()->user->setFlash('errorFlash','Failed to create user. Username or email already exists.');
                            $this->refresh();
                        }
                    }
                    else{
                        Yii::app()->user->setFlash('errorFlash','Data not valid. Username, password, and email Required, or email format not valid.');
                        $this->refresh();
                    }
                }
            }
            $this->render('userCreate', array('model'=>$model));
        }
        
        public function actionUserList(){
            $model = new UserForm;
            $list = $model->getUserList();
            $this->render("userList", array('list'=>$list));
        }
        
        public function actionProfile(){
            $model = new UserForm;
            $profile = $model->getProfileByName(Yii::app()->user->name);
            $this->render("profile", array('profile'=>$profile));
        }
        
        public function actionEditProfile(){	//?r=cms/editProfile
            $model = new UserForm;							//load mmodel
            $id = Yii::app()->getRequest()->getParam('id');	//dapetin id profile dengan method $_GET
            $profile = $model->getProfileById($id);			//dapetin profile dari database
            $name = "";
            $error = 0; 
            if(count($profile) > 0) {$name = $profile[0]['username'];}	//dapetin nama usernmame
            if(!(Yii::app()->user->superuser == 0 && $name != Yii::app()->user->name)){	 //yang bisa edit hanya user itu sendiri dan superuser
                $model->setProfile($id);	//set id di model
                if(isset($_POST['UserForm'])){
                    $flag=true;
                    $model->attributes = $_POST['UserForm']; 			//set attribute
                    $model->passFlag = $_POST['UserForm']['passFlag'];	//passflag untuk new password (2) ato old password (1)
                    if($model->passFlag == 2){ 		//new password
                        if($_POST['UserForm']['newPassword'] != null && ($_POST['UserForm']['newPassword'] == $_POST['UserForm']['confNewPassword'])){
                            $model->password = md5($_POST['UserForm']['newPassword']); $flag = true;}		//ganti password di model
                        else{$flag=false;}
                    }
                    if($model->validate() && $flag){	//klo data valid dan password baru ada
                        $model->updateAccount($id);		//update di database
                        Yii::app()->user->setFlash('successFlash','Success to edit profile.');
                        //$this->refresh();
                    }
                    else{
                        Yii::app()->user->setFlash('errorFlash','Edit profile Failed. Some data not valid.');
                        //$this->refresh();
                    }
                }
            }
            else{
                $error = 1;
            }
            $this->render("editProfile", array('model'=>$model, 'error'=>$error));
        }
        
        public function actionDeleteAccount(){	//?r=cms/deleteAccount&id=...
            $model = new UserForm;	//load model
            $id = Yii::app()->getRequest()->getParam('id');	//dapetin id dengan method $_GET
            if(Yii::app()->user->superuser == 1 && $id!=null){	//yang bisa delete account hanya superuser :)
                $model->deleteAccount($id);
            }
            $list = $model->getUserList();
            $this->redirect("?r=cms/userList", array('list'=>$list));	//redirect
        }
        
        public function actionApplicationIcon(){	//?r=cms/applicationIcon
            $model = new ThemeForm;						//load model
            $themeList = $model->getAllThemes(); 		//ambil semua theme yang ada di table
            $choosenTemplate = $model->templateID; 		//klo belum ada sebelumnya = 0
            $prevIcon = $model->prevIcon;				//klo ada icon sebelumnya
			$prevSplash = $model->prevSplashScreen;		//klo ada splash screen sebelumnya
            if(isset($_POST['ThemeForm'])){
                $model->attributes = $_POST['ThemeForm'];  					//set attribute ke model
                $model->icon = CUploadedFile::getInstance($model,'icon'); 	//set logo dengan ctype
				$model->splashScreen = CUploadedFile::getInstance($model,'splashScreen'); 	//set splash screen dengan ctype
                if(!isset($_POST['ThemeForm']['templateID']) || $_POST['ThemeForm']['templateID'] == null){ 	//klo ga pilih template jika sebelumnya 0
                    Yii::app()->user->setFlash('errorFlash','Please choose a theme.'); 	//set error
                    $this->refresh();
                }
                else { 	//uda pilih template
					if($model->validate()){ 	//app name dan app short name sudah diisi
						$model->templateID = $_POST['ThemeForm']['templateID']; 	//ambil template id yang dipilih
						$iconn = null; 		//buat nampung path logo
						$splash = null; 	//buat nampung path splash screen
						
						if($model->icon != null){ //upload logo baru
							$bspath = YiiBase::getPathOfAlias('webroot').'/images';	//set path tempat upload
							$ext = $model->icon->getExtensionName(); 				//ambil extension gambar
							$filename = "apps_icon_template.".$ext;					//file name default
							$path = $bspath."/".$filename;							//path upload + file name
							$model->icon->saveAs($path,true);						//upload gambar
							$iconn = Yii::app()->request->hostInfo.Yii::app()->baseUrl."/images/".$filename;	//path logo
						}
						else{ 		//ga upload logo baru -> pake logo sebelumnya (prevIcon)
							$iconn = $_POST['ThemeForm']['prevIcon'];
						}
						
						if($model->splashScreen != null){  //upload splash screen baru
							$bspath = YiiBase::getPathOfAlias('webroot').'/images';	//set path tempat upload
							$ext = $model->splashScreen->getExtensionName(); 		//ambil extension gambar
							$filename = "apps_splash_template.".$ext;				//file name default
							$path = $bspath."/".$filename;							//path upload + file name
							$model->splashScreen->saveAs($path,true);				//upload gambar
							$splash = Yii::app()->request->hostInfo.Yii::app()->baseUrl."/images/".$filename;	//path splash screen
						}
						else{ 		//ga upload splash screen baru -> pake prev splash screen image
							$splash = $_POST['ThemeForm']['prevSplashScreen'];
						}
						
						$model->updateAppsIcon($iconn, $splash);		//update database
						Yii::app()->user->setFlash('successFlash','Success to update Application Icon.');	//kasih pesan success
						$this->refresh();
					}
					else{	//klo app name atau app short name kosong
						Yii::app()->user->setFlash('errorFlash','Application Name or Application Short Name must not null.');	//pesan error
						$this->refresh();
					}
                }
            }
			//render page
            $this->render("applicationIcon", array('model'=>$model, 'themeList'=>$themeList, 'choosenTemp'=>$choosenTemplate, 'prevIcon'=>$prevIcon, 'prevSplash'=>$prevSplash));
        }
        
        public function actionBuyIcon() 	//?r=cms/buyIcon
        {
                $model = new BuyIconForm;
                $data = $model->getList();
                $this->render('BuyIcon',array('model'=>$model,'data'=>$data));
        }
        
        protected function shortenURL($url){	//function untuk create TINY URL
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url='.$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,$timeout);
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
        }
        
        protected function sendEmail($subject, $bodyText, $to){	//function untuk kirim EMAIL
            $message = new YiiMailMessage;
            $message->subject = $subject;
            $message->setBody($bodyText, 'text/html');                
            $message->addTo($to);
            $message->from = "do.notreply@teltics.com";
            Yii::app()->mail->send($message);
        }
        
        protected function notifByAndroid($register_id, $title_content, $content_id){	//function untuk kirim notifikasi ke android
            $url = $URLMANIC['base'].'?AppName='.$URLMANIC['ios']['android'].'&Token='.$register_id.'&Env='.$URLMANIC['android']['env'].'&data.message='.urlencode($title_content).'&data.content_id='.$content_id;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }
        
        protected function notifByIos($register_id, $title_content, $content_id){	//function untuk kirim notofikasi ke iOS
            $params = array('aps' => array('alert' => $title_content));
            $url = $this->URLMANIC['base'].'?AppName='.$this->URLMANIC['ios']['appsname'].'&Token='.$register_id.'&Env='.$this->URLMANIC['ios']['env'].'&Message='.base64_encode(json_encode($params)).'&MessageEncoding=Base64‏';
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }
		
        public function actionBroadcast()	//?r=cms/broadcast
        {
            $model = new BroadcastForm();
            $data = $model->getUser();	
            $contentId = $_POST['contentId'];
            $contentTitle = $_POST['contentTitle'];
            foreach($data as $x)
            {
                if($x['platform'] == 'android'){
                    //$result = $this->notifByAndroid($x['regid'],$contentTitle,$contentId);
                    $model->insertNotif($x['regid'],$x['platform'],$contentId);
					print_r("Notification sent!");
                }
                else{
                    //$result = $this->notifByIos($x['regid'],$contentTitle,$contentId);
					$model->insertNotif($x['regid'],$x['platform'],$contentId);
					print_r("Notification sent!");
                }
            }
        }
}
?>
