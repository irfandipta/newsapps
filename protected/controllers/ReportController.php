<?php

class ReportController extends Controller{
	public function init(){
		if(Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->user->returnUrl.'?r=site/login');
	}
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionStatistics(){
		$model = new Report;
		$this->render('statistics');
	}
	
	public function actionStatistic1(){
		$model = new Report;
		$this->render('statistic1');
	}
	
	public function actionStatistic2(){
		$model = new Report;
		$this->render('statistic2');
	}
	
	public function actionStatistic3(){
		$model = new Report;
		$this->render('statistic3');
	}
	
	public function actionChart1(){
		$model = new Report;
		$mergedData = array();
		$startDate = Yii::app()->getRequest()->getParam('startDate');
		$endDate = Yii::app()->getRequest()->getParam('endDate');
		$data1 = $model->listAppUser($startDate, $endDate);
		///*
		$x = strtotime("-1 day", strtotime($startDate));
		while($x != strtotime($endDate)){
			$x += 86400; //+1 day
			$y = 0;
			foreach($data1 as $r){
				if(strtotime($r['created']) == $x){ $y = (int)$r['count']; } //tanggal ada di database, count = count dari database
			}
			$data[] = array (($x*1000), $y);
		}
		$mergedData[] =  array('label' => "" , 'data' => $data, 'color' => '#241CFF');
		//*/
		/*
		$data1 = $model->listAppUser($startDate, $endDate);
		foreach($data1 as $r){
			$x = strtotime($r['created'])*1000;
			$y = (int)$r['count'];
			$data1[] = array ($x, $y);
		}
		$mergedData[] =  array('label' => "iOS" , 'data' => $data1, 'color' => '#FFCC24');
		//*/
		echo json_encode($mergedData);
	}
	
	public function actionChart2(){
		$model = new Report;
		$mergedData = array();
		$startDate = Yii::app()->getRequest()->getParam('startDate');
		$endDate = Yii::app()->getRequest()->getParam('endDate');
		///*
		$dataA = $model->listAppUserByOS("android", $startDate, $endDate);
		$x1 = strtotime("-1 day", strtotime($startDate));
		while($x1 != strtotime($endDate)){
			$x1 += 86400; //+1 day
			$y = 0;
			foreach($dataA as $r){
				if(strtotime($r['created']) == $x1){ $y = (int)$r['count']; } //tanggal ada di database, count = count dari database
			}
			$data1[] = array (($x1*1000), $y);
		}
		$mergedData[] =  array('label' => "Android" , 'data' => $data1, 'color' => '#241CFF');
		
		$dataB = $model->listAppUserByOS("ios", $startDate, $endDate);
		$x2 = strtotime("-1 day", strtotime($startDate));
		while($x2 != strtotime($endDate)){
			$x2 += 86400; //+1 day
			$y = 0;
			foreach($dataB as $r){
				if(strtotime($r['created']) == $x2){ $y = (int)$r['count']; } //tanggal ada di database, count = count dari database
			}
			$data2[] = array (($x2*1000), $y);
		}
		$mergedData[] =  array('label' => "iOS" , 'data' => $data2, 'color' => '#FFCC24');
		//*/
		/*
		$data1 = $model->listAppUserByOS("android", $startDate, $endDate);
		foreach($data1 as $r){
			$x = strtotime($r['created'])*1000;
			$y = (int)$r['count'];
			$data1[] = array ($x, $y);
		}
		$mergedData[] =  array('label' => "Android" , 'data' => $data1, 'color' => '#FFCC24');
		
		$data2 = $model->listAppUserByOS("ios", $startDate, $endDate);
		foreach($data2 as $r){
			$x = strtotime($r['created'])*1000;
			$y = (int)$r['count'];
			$data2[] = array ($x, $y);
		}
		$mergedData[] =  array('label' => "iOS" , 'data' => $data2, 'color' => '#241CFF');
		//*/
		
		echo json_encode($mergedData);
	}
	
	public function actionChart3(){
		$model = new Report;
		$mergedData = array();
		$startDate = Yii::app()->getRequest()->getParam('startDate');
		$endDate = Yii::app()->getRequest()->getParam('endDate');
		$data1 = $model->listInstallByOS("android", $startDate, $endDate);
		foreach($data1 as $r){
			$x = 0;
			$y = (int)$r['count'];
			$data1[] = array ($x, $y);
		}
		$mergedData[] =  array('label' => "Android" , 'data' => $data1, 'color' => '#241CFF');
		$data2 = $model->listInstallByOS("ios", $startDate, $endDate);
		foreach($data2 as $r){
			$x = 1;
			$y = (int)$r['count'];
			$data2[] = array ($x, $y);
		}
		$mergedData[] =  array('label' => "iOS" , 'data' => $data2, 'color' => '#FFCC24');
		
		echo json_encode($mergedData);
	}
}

?>