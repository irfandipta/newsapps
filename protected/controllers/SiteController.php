<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
        
        public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('login','view','UserActivation','logout','reset','ConfReset'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$model = new IndexForm;
		$AndroidUser = $model->getUserAndroid();
		$IosUser = $model->getUserIos();
		$countAndroidUser = count($AndroidUser);
		$countIosUser = count($IosUser);
		$dataNotif = $model->getNotif();
		if($dataNotif==NULL)
		{
			$isEmpty = 1;
			$this->render('index',array('countAndroidUser'=>$countAndroidUser,'countIosUser'=>$countIosUser,'isEmpty'=>$isEmpty));
		}
		else
		{
			$isEmpty = 0;
			$lastNotif = $dataNotif[0]['created'];
			$lastNotifDateTime = strtotime($lastNotif);
			$lastNotifDate = date('d-m-Y', $lastNotifDateTime);
			$contentDetail = $model->getContentTitle($dataNotif[0]['content_id']);
			$contentTitle = $contentDetail[0]['news_title'];
			$this->render('index',array('countAndroidUser'=>$countAndroidUser,'countIosUser'=>$countIosUser,'lastNotifDate'=>$lastNotifDate,'contentTitle'=>$contentTitle, 'isEmpty'=>$isEmpty));
		}
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
                $this->layout = '//layouts/login';
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
                        $model->password = md5($model->password);
                        $stat = $model->flagStatus();
			// validate user input and redirect to the previous page if valid
			if($model->validate()){
                            if($stat == 0){ //tidak aktif
                                Yii::app()->user->setFlash('errorFlash','Account not active. Please check your email or contact the Admin.');
                                $this->refresh();
                            }
                            else if($stat == 1){    //aktif
                                if($model->login()){
                                    $this->redirect(Yii::app()->user->returnUrl);
                                }
                            }
                        }
				
		}
		// display the login form
                
		$this->render('login',array('model'=>$model));
	}
        
        public function actionComment($id){
            $this->layout = false;
            $this->render('comment' , array('id'=> $id));
        }
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
        
        public function actionView(){
            $this->layout = '//layouts/view';
            $model = new ContentdetailForm;
            $data =null; $image = null; $title = null;
            $ids = Yii::app()->getRequest()->getParam('id');
            if($ids != null){
                $data = $model->getNewsDetail($ids);
                $title = $model->getTitle();
                $image = $model->getImagePath();
            }
            $this->render('view' , array('data'=> $data, 'title'=>$title, 'icon'=>$image));
        }
        
        public function actionUserActivation(){
            $model = new UserForm;
	    $model2 = new LoginForm;
            $this->layout = '//layouts/login';
            $userIDHash = Yii::app()->getRequest()->getParam('id');
            $change = $model->changeStatus($userIDHash);
            if($change == 1){   //berhasil diaktivasi
                Yii::app()->user->setFlash('successFlash','Success to activate your Account. Please Login.');
            }
            else if($change == 2){  //account sudah aktif
                Yii::app()->user->setFlash('errorFlash','Your account already active');
            }
            else if($change == 3){  //account tidak ditemukan
                Yii::app()->user->setFlash('errorFlash','Account not Exists');
            }
            $this->redirect('?r=site/login',array('model'=>$model2));
        }
        
        public function actionReset()
	 {
		$this->layout = '//layouts/login';
		$model = new ResetForm;
		if(isset($_POST['ResetForm']))
		{
			$model->attributes = $_POST['ResetForm'];
			$model->checkEmail();
			$this->refresh();
		}
		$this->render('reset', array('model'=>$model));
	 }
	 
	 public function actionConfReset()
	 {
		$this->layout = '//layouts/login';
		$id = Yii::app()->getRequest()->getParam('id');
		$model = new ResetForm;
		$model->getData($id);
		if(isset($_POST['ResetForm']))
		{
			$model->attributes = $_POST['ResetForm'];
			if($model->password == $model->passwordconf)
			{
				$model->updatePass($id);
				Yii::app()->user->setFlash('successFlash','Your password has been successfuly updated!');
				$this->redirect(Yii::app()->user->returnUrl.'?r=site/login');
			}
			else
			Yii::app()->user->setFlash('errorFlash','Your password and password confirmation does not match!');
		}
		$this->render('resetconf',array('model'=>$model));
	 }
}