<?php

class ReminderController extends Controller{
	public function init(){
		if(Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->user->returnUrl.'?r=site/login');
	}
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionList(){
		$model = new Reminder;
		$id = Yii::app()->getRequest()->getParam('id');
		if($id == null) {$id = 0;}
		$reminder = $model->getListReminder();
		$this->render('listReminder', array('reminder'=>$reminder, 'id'=>$id));
	}
	
	public function actionCreate(){
		$this->layout= '//layouts/view'; 
		$model = new Reminder;
		$userList = $model->getListUser();
		if(isset($_POST['Reminder'])){
			$toID = $_POST['Reminder']['toID'];
			$msg = $_POST['Reminder']['message'];
			if($msg != "") {
				$model->setData($toID, $msg);
				$insert = $model->insertReminder();
			}
			$this->redirect("?r=reminder/list");
		}
		$this->render('createReminder', array('model'=>$model, 'userList'=>$userList));
	}
	
	public function actionDelete(){
		$model = new Reminder;
		$id = Yii::app()->getRequest()->getParam('taskid');
		$delete = $model->deleteReminder($id);
		$this->redirect("?r=reminder/list");
		//echo $_GET['taskid'];
	}
	
	public function actionComment(){
		$this->layout= '//layouts/view'; 
		$model = new Reminder;
		$parentID = Yii::app()->getRequest()->getParam('parentId');
		$comm = $model->getReminderComment($parentID);
		//print_r($comm);
		echo $this->renderPartial('listComment', array('parentID'=>$parentID, 'comment'=>$comm));
	}
	
	public function actionAddComment(){
		$model = new Reminder;
		$parentID = Yii::app()->getRequest()->getParam('parentId');
		$to = Yii::app()->getRequest()->getParam('to');
		$msg = Yii::app()->getRequest()->getParam('msg');
		if($msg != ''){
			$add = $model->addComment($parentID, $to, $msg);
		}
	}
	
	public function actionDone(){
		$model = new Reminder;
		$id = Yii::app()->getRequest()->getParam('id');
		$done = Yii::app()->getRequest()->getParam('done');
		$change = $model->setDone($done, $id);
	}
	
	public function actionRead(){
		//?r=reminder/read&parentId='+id+'&from='+from+'&to='+to
		$model = new Reminder;
		$id = Yii::app()->getRequest()->getParam('parentId');
		$from = Yii::app()->getRequest()->getParam('from');
		if($from == $model->userID){
			$read = $model->setRead($id);
		}
	}
	
	public function actionDashboard(){
		$this->layout= '//layouts/view'; 
		$model = new Reminder;
		$db = $model->getDashboardReminder();
		echo $this->renderPartial('reminderDashboard', array('db'=>$db));
		//echo $this->render('reminderDashboard', array('db'=>$db));
	}
}

?>