<?php

class APIController extends Controller
{
    public function actionContent(){
        $model = new APIContent;
        $data = null;
        $categoryID = Yii::app()->getRequest()->getParam('cat');
        $contentID = Yii::app()->getRequest()->getParam('id');
        if($categoryID == null && $contentID == null){
            $data = $model->latestContent();
        }
        else{
            if($categoryID != null){
                $data = $model->contentByCategory($categoryID);
            }
            if($contentID != null){
                $data = $model->contentByContentID($contentID);
            }
        }
        echo json_encode($data);
        die();
    }
    
    public function actionContentSearch(){
        $model = new APIContent;
        $data = null;
        //Contoh Link : http://localhost/yii/newsapps/?r=API/contentSearch&cat=1&key=ayam&start=2014-07-16&end=2014-07-18
        $searchKey = urldecode(Yii::app()->getRequest()->getParam('key'));
        $categoryID = Yii::app()->getRequest()->getParam('cat');
        $dateStart = Yii::app()->getRequest()->getParam('start');
        $dateEnd = Yii::app()->getRequest()->getParam('end');
		if($searchKey != null){ 
			if($categoryID == null && $dateStart == null && $dateEnd == null){ //by searchKey saja
				$data = $model->contentSearchAll($searchKey);
			}
			else if($categoryID != null && $dateStart != null && $dateEnd != null){ 
				if($categoryID == 0){ //by all category and date
					$data = $model->contentSearchAllCat($searchKey, $dateStart, $dateEnd);
				}
				else{ //by a category and date
					$data = $model->contentSearchByCat($searchKey, $categoryID, $dateStart, $dateEnd);
				}
			}
		}
		else{
			$data = array('error' => 1, 'message' => 'Search Key must not null');
		}
        echo json_encode($data);
        die();
    }
    
    public function actionCountView(){
        $model = new APIContent;
        $count = 0;
        //Contoh Link = http://localhost/yii/newsapps/?r=API/CountView&id=2
        $contentID = Yii::app()->getRequest()->getParam('id');
        if($contentID != null){
            $count = $model->countView($contentID);
            $result = array('error' => 0, 'count' => $count);
        }else{
            $result = array('error' => 1, 'message' => 'Content id must not null');
        }
        echo json_encode($result);
        die();
    }
    
    public function actionCategory(){
	$model = new APICategoryForm();
	$result = $model->getCategoryList();
	echo json_encode($result);
    }
	
    public function actionSubCategory(){
	$model = new APICategoryForm();
	$parentId = Yii::app()->getRequest()->getParam('parentId');
	$result = $model->getSubCategoryList($parentId);
	echo json_encode($result);
    }
    
    public function actionGcm(){
        $model = new APIGcmForm;
        $token = Yii::app()->getRequest()->getParam('token');
        $platform = Yii::app()->getRequest()->getParam('platform');
        if($token != NULL && $platform != NULL)
        {
            if(count($model->searchGcm($token)) == 0){
                $model->insertGcm($token,$platform);
                $result = array('error' => 0);
            }else{
                $result = array('error' => 1, 'message' => 'Token already registered');
            }
        }
        else{
            $result = array('error' => 1, 'message' => 'Token and Platform must not null');
        }
        echo json_encode($result);
        die();
    }
    
    public function actionUpdateLeaderboard(){
        //contoh link : http://localhost/yii/newsapps/?r=API/UpdateLeaderboard&regid=abc&dist=40&time=3
        $model = new APILeaderboard;
        $regid = Yii::app()->getRequest()->getParam('regid');
        $distance = Yii::app()->getRequest()->getParam('dist');
        $time = Yii::app()->getRequest()->getParam('time');
        if($regid != null && $distance != null && $time != null){
            $message = $model->updateData($regid, $distance, $time);
            $result = array('error' => 0, 'message' => $message);
        }
        else{
            $result = array('error' => 1, 'message' => 'regid, distance, and time must not null');
        }
        echo json_encode($result);
        die();
    }
    
    public function actionListRanking(){
        //contoh link: http://localhost/yii/newsapps/?r=API/ListRanking
        $model = new APILeaderboard;
        $data = $model->listRanking();
        echo json_encode($data);
    }
    
    public function actionChangeUserName(){
        //contoh link: http://localhost/yii/newsapps/?r=API/ChangeUserName&regid=abc&uname=ayam
        $model = new APILeaderboard;
        $uname = Yii::app()->getRequest()->getParam('uname');
        $regid = Yii::app()->getRequest()->getParam('regid');
        if($uname != null && $regid != null){
            $model->changeUserName($regid, $uname);
            $result = array('error'=>0);
        }
        else{
            $result = array('error'=>1, 'message'=>'regid and username must not null');
        }
        echo json_encode($result);
        die();
    }
}

