CREATE TABLE tbl_leaderboard(
id INT AUTO_INCREMENT PRIMARY KEY,
user_id INT NOT NULL UNIQUE,
distance INT,
time INT,
speed FLOAT,
updated TIMESTAMP,
FOREIGN KEY (user_id) REFERENCES ap_user_gcm(id)
) ENGINE=InnoDB;