<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - User Statistic';
?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ; ?>/js/flotChart/jquery.flot.js" ></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ; ?>/js/flotChart/jquery.flot.time.js" ></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ; ?>/js/flotChart/jquery.flot.axislabels.js" ></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">

<script type="text/javascript">
	$(document).ready(function(){
		var startDate = document.getElementById("datepicker1").value;
		var endDate = document.getElementById("datepicker2").value;
		var startSeries = new Date(startDate).getTime();
		var endSeries = new Date(endDate).getTime();
		
		function chart(){
			$.ajax({
				url: "<?php echo Yii::app()->request->hostInfo.Yii::app()->baseUrl;?>?r=report/chart1&startDate="+startDate+"&endDate="+endDate,
				method: 'GET',
				dataType: 'json',
				beforeSend: function() { $("#loadingContainer").show(); document.getElementById("placeholder").style.visibility="hidden"; }, 
				complete: function() { $("#loadingContainer").hide(); document.getElementById("placeholder").style.visibility="visible"; },
				success: function(data) {
							var length = data.length;
							var finalData = data;
							var options = {
								xaxis: { mode: "time" , min: startSeries, max: endSeries, axisLabel: "Date" , axisLabelUseCanvas: true, axisLabelFontSizePixels: 12, axisLabelFontFamily: 'Verdana, Arial', axisLabelPadding: 5 },
								yaxis: { min: 0 , axisLabel: "User Count" , axisLabelUseCanvas: true, axisLabelFontSizePixels: 12, axisLabelFontFamily: 'Verdana, Arial', axisLabelPadding: 5 } ,
								legend: { show: false },
								lines: { show: true },
								points: { show: true, hoverable:true },
								grid: { hoverable: true }
							};
							$.plot($("#placeholder"), finalData, options);
						},
				error: function(e) { console.log(e.message); }
			});
		}
		
		$("#datepicker1, #datepicker2").change(function(){
			startDate = document.getElementById("datepicker1").value;
			endDate = document.getElementById("datepicker2").value;
			startSeries = new Date(startDate).getTime();
			endSeries = new Date(endDate).getTime();
			chart();
		});
		
		//tooltip
		var previousPoint = null;	 
		$.fn.UseTooltipDate = function () {
			$(this).bind("plothover", function (event, pos, item) {                         
				if (item) {
					if (previousPoint != item.dataIndex) {
						previousPoint = item.dataIndex;
						$("#tooltip").remove();
						var x = new Date(item.datapoint[0]);
						var y = item.datapoint[1];  
						var months = ['January','February','March','April','May','Juny','July','August','September','October','November','December'];
						var dd = x.getDate() + " " + months[x.getMonth()] + " " + x.getFullYear();
						showTooltip(item.pageX, item.pageY, item.series.label + " User at " + dd + " = <b>" + y + "</b>");
					}
				}
				else { $("#tooltip").remove();	previousPoint = null; }
			});
		};
		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css({
				position: 'absolute', display: 'none', top: y + 5, left: x + 20, border: '2px solid #4572A7', padding: '2px',     
				size: '12', 'border-radius': '6px 6px 6px 6px', 'background-color': '#fff',	opacity: 0.80
			}).appendTo("body").fadeIn(200);
		}
		
		//onload		
		chart(); $("#placeholder").UseTooltipDate(); 
	});
	
	$(function() {
		$( "#datepicker1" ).datepicker({dateFormat:"yy-mm-dd", changeMonth: true, changeYear: true}); // yy-mm-dd d MM yy
		$( "#datepicker2" ).datepicker({dateFormat:"yy-mm-dd", changeMonth: true, changeYear: true});
	});
	</script>

<div class="contentTitleBox">
    USER STATISTICS
</div>

<div class="form-createContent">
	<div style="width:100%; font-size:14px; margin-top:-10px;">
		<div style="width:49%; float:left; padding: 3px;">
			<div style="float:right;">
				Start Date<br/>
				<input type="text" id="datepicker1" value="<?php echo date('Y-m-d',strtotime('-1 week'));?>" class="input-text" style="width:100px;" readonly/>
			</div>
		</div>
		<div style="width:49%; float:left; padding: 3px;">
			End Date<br/>
			<input type="text" id="datepicker2" value="<?php echo date('Y-m-d');?>" class="input-text" style="width:100px;" readonly/>
		</div>
	</div>
	<div class="statisticBox">
		<div class="statistic">
			<div class="stat-container">
				<div id="loadingContainer"><img src="<?php echo Yii::app()->theme->baseUrl."/images/loading.gif";?>" class="loadingImg"/></div>
				<div id="placeholder" class="stat-placeholder"></div>
			</div>
		</div>
	</div>
</div>