<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
?>

<?php if(Yii::app()->user->hasFlash('errorFlash')): ?>
    <div class="flash-error-custom">
        <?php echo Yii::app()->user->getFlash('errorFlash'); ?>
    </div>
<?php endif; ?>
    
    <?php if(Yii::app()->user->hasFlash('successFlash')): ?>
    <div class="flash-success-custom">
        <?php echo Yii::app()->user->getFlash('successFlash'); ?>
    </div>
<?php endif; ?>

<div class="form-login">
    <div class="login-title">RESET PASSWORD</div>

    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'login-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                    'validateOnSubmit'=>true,
            ),
    )); ?>
	<div class="rowfull">
        <?php echo 'username: '.$model->username; ?>
    </div>
    <div class="rowfull">
        <?php echo $form->textField($model,'password', array('placeholder'=> 'new password' , 'class' => 'input-text')); ?>
    </div>
    <div class="rowfull">
        <?php echo $form->error($model,'password'); ?>
    </div>
	<div class="rowfull">
        <?php echo $form->textField($model,'passwordconf', array('placeholder'=> 'new password confirmation' , 'class' => 'input-text')); ?>
    </div>
    <div class="rowfull">
        <?php echo $form->error($model,'passwordconf'); ?>
    </div>
    <div class="rowfull">
        <?php echo CHtml::submitButton('SUBMIT' , array('class' => 'input-submit-right')); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
<?php /*
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
		<p class="hint">
			Hint: You may login with <kbd>demo</kbd>/<kbd>demo</kbd> or <kbd>admin</kbd>/<kbd>admin</kbd>.
		</p>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Login'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
*/ ?>