<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<script>
	$(document).ready(function(){
		$.ajax({
			url: '<?php echo Yii::app()->request->hostInfo.Yii::app()->baseUrl;?>?r=reminder/dashboard',
			success: function(result) {	$('#reminder').html(result); },
			error: function(e) { console.log(e.message); }
		});
	});
</script>

<?php
	/*if($isEmpty == 1)
	echo "No Notification Found"."<br/>";
	else
	{
		echo "Last Push Notification:"."<br/>";
		echo "Date = ".$lastNotifDate."<br/>";
		echo "Content Title = ".$contentTitle."<br/>";
	}
	echo "Android User Total = ".$countAndroidUser."<br/>";
	echo "IOS User Total = ".$countIosUser."<br/>";
	*/
?>
<div style="width:1000px; float:left;">

<div style="width:48%; margin: 10px; float:left;">
	<div style="width:100%; float:left; margin-bottom:10px;">
		<div style="width:100%; float:left; font-size:18px; font-weight:bold;">Notification</div>
		<div style="width:98%; float:left; max-height:300px; min-height:auto; overflow:auto; padding:5px; border-radius:4px; border:1px solid black; background-color:white;">
		<?php
			if($isEmpty == 1) echo "<center>No Notification Found</center>";
			else
			{
				echo "Last Push Notification:"."<br/>";
				echo "Date = ".$lastNotifDate."<br/>";
				echo "Content Title = ".$contentTitle."<br/>";
			}
		?>
		</div>
	</div>
	<div style="width:100%; float:left; margin-bottom:10px;">
		<div style="width:100%; float:left; font-size:18px; font-weight:bold;">Total User</div>
		<div style="width:100%; float:left; height:68px; min-height:auto; overflow:auto; border-radius:4px; border:1px solid black; background-color:white;">
			<div style="width:49%; float:left; padding-top:7px;" title="Android">
				<div style="float:right; margin:0px 10px;"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ico-android.png" style="width:50px; height:50px;" /></div>
				<div style="float:right; padding:10px;"><b style="font-size:20px;"><?php echo $countAndroidUser;?></b></div>
			</div>
			<div style="width:0.2%; float:left; background-color:black; height:100%;">&nbsp;
			</div>
			<div style="width:49%; float:left; padding-top:7px;" title="iOS">
				<div style="float:left; margin:0px 10px; margin-top:-3px; "><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ico-ios.png" style="width:50px; height:50px; float:left; margin:5px;" /></div>
				<div style="float:left; padding:10px;"><b style="font-size:20px;"><?php echo $countIosUser;?></b></div>
			</div>
		</div>
	</div>
</div>

<div style="width:48%; margin: 10px; float:left;">
	<div style="width:100%; float:left; font-size:18px; font-weight:bold;">Reminder</div>
	<div id="reminder" style="width:100%; float:left; max-height:600px; min-height:auto; overflow:auto; border-radius:4px; border:1px solid black; background-color:white;"></div>
</div>

</div>