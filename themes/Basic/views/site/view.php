<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - View';
?>

    <?php
    if(count($data) > 0):
        if($icon != null):  
    ?>
        <div class="imgContentContainer">
            <img src="<?php echo $icon; ?>" alt="View" class="imgContentView"/>
        </div>
        <?php endif;?>

        <?php
            if($data[0]['content_detail'] != null):
        ?>
        <div class="detailContentContainer">
            <div class="detailContentTitle"><?php echo strtoupper($title);?></div>
            <?php echo $data[0]['content_detail'];?>
        </div> 
        <?php endif; ?>
    <?php endif; ?>
