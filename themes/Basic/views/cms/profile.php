<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name. " - Profile";
?>
<div class="contentTitleBox">
    <div class="only_title"> PROFILE - <?php echo strtoupper($profile[0]['username']);?></div>
	<div class="only_edit_profile" style="float: right;">
		<?php 
            $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'buyicon-form',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array('validateOnSubmit'=>true,),
                    'action'=>Yii::app()->createUrl('cms/EditProfile',array('id'=>$profile[0]['id'])),
            )); 
        ?>
        <button><i><img src="<?php echo Yii::app()->theme->baseUrl;?>/images/edit.png" style="width:15px;height:15px;"/></i> Edit Profile</button>
        <?php
            $this->endWidget(); 
        ?> 
	</div>
</div>

<div class="form-createContent">
    <div class="rowfull general_background">
        <table>
            <tr>
                <td style="width:25%">Username</td>
                <td style="width:5%">:</td>
                <td style="width:70%"><?php echo "<b>".strtoupper($profile[0]['username'])."</b>";?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td><?php echo $profile[0]['email'];?></td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:</td>
                <td><?php if($profile[0]['status']==1) echo "Active"; else echo "Not Active";?></td>
            </tr>
            <tr>
                <td>Super User</td>
                <td>:</td>
                <td><?php if($profile[0]['superuser']==1) echo "Yes"; else echo "No";?></td>
            </tr>
            <tr>
                <td>Created Date</td>
                <td>:</td>
                <td><?php  echo $profile[0]['create_at'];?></td>
            </tr>
            <tr>
                <td>Last Visit</td>
                <td>:</td>
                <td><?php if($profile[0]['lastvisit_at']=="0000-00-00 00:00:00") echo "-"; else echo $profile[0]['lastvisit_at'];?></td>
            </tr>
        </table>
        <div style="margin:10px 30px; margin-top:-5px; font-size:16px;">
        
        </div>
    </div>
</div>