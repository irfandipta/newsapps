<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - Create User';
?>

<?php if(Yii::app()->user->hasFlash('errorFlash')): ?>
    <div class="flash-error-custom">
        <?php echo Yii::app()->user->getFlash('errorFlash'); ?>
    </div>
<?php endif; ?>
    
    <?php if(Yii::app()->user->hasFlash('successFlash')): ?>
    <div class="flash-success-custom">
        <?php echo Yii::app()->user->getFlash('successFlash'); ?>
    </div>
<?php endif; ?>

<div class="contentTitleBox">
    CREATE USER
</div>

<div class="form-createContent">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'usercreate-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array('validateOnSubmit'=>true,),
    )); 
    ?>
    
    <div class="row75">
        <?php echo $form->labelEx($model,'username', array('class'=>'custom-label')); ?>
        <?php echo $form->textField($model,'username', array('class' => 'input-text')); ?>
    </div>
    
    <div class="row75">
        <?php echo $form->labelEx($model,'password', array('class'=>'custom-label')); ?>
        <?php echo $form->passwordField($model,'password', array('class' => 'input-text')); ?>
    </div>
    
    <div class="row75">
        <?php echo $form->labelEx($model,'confirmPassword', array('class'=>'custom-label')); ?>
        <?php echo $form->passwordField($model,'confirmPassword', array('class' => 'input-text')); ?>
    </div>
    
    <div class="row75">
        <?php echo $form->labelEx($model,'email', array('class'=>'custom-label')); ?>
        <?php echo $form->emailField($model,'email', array('class' => 'input-text')); ?>
    </div>
    
    <div class="row75">
        <?php echo CHtml::submitButton('CREATE', array('class'=>'btnSubmitContent'));?>
    </div>
    <?php $this->endWidget(); ?>
</div>