<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - Create Category';
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">

<?php if(Yii::app()->user->hasFlash('errorFlash')): ?>
    <div class="flash-error-custom">
        <?php echo Yii::app()->user->getFlash('errorFlash'); ?>
    </div>
<?php endif; ?>
    
    <?php if(Yii::app()->user->hasFlash('successFlash')): ?>
    <div class="flash-success-custom">
        <?php echo Yii::app()->user->getFlash('successFlash'); ?>
    </div>
<?php endif; ?>

<div class="contentTitleBox">
    CREATE CATEGORY
</div>

<div class="form-createContent">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'category-create-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array('validateOnSubmit'=>true,),
            'htmlOptions'=>array('enctype'=>'multipart/form-data',),
    )); 
    ?>
    
    <div class="row75">
        <?php echo $form->labelEx($model,'category name', array('class'=>'custom-label')); ?>
        <?php echo $form->textField($model,'categoryName', array('class' => 'input-text')); ?>
		<?php echo $form->error($model,'categoryName')."<br/>"; ?>
	</div>
	<div class="space10"></div>
    <div class="row75 custom-label">
        Image *<br/>
        <a href="#" id="klikUpload" onclick="showit(0)">Upload</a> |  <a onclick="showit(1)" href="#" id="klikUrl">URL</a>
    </div>
	<div class="row75" id="forUploadcategory">
        <?php echo CHtml::activeFileField($model,'pic')."<br/>";?>
    </div>
	<div id="forUrlcategory" class="row75">
		<?php echo $form->textField($model,'picUrl', array('class' => 'input-text','style'=>'width:98.5%'))."<br/>"; ?>
	</div>
	<div class="space10"> </div>

	<div class="row75">
		<div class="label">
			<?php echo $form->labelEx($model,'Created By',array('class'=>'custom-label'))."<br/>"; ?>
		</div>
			<?php echo $form->textField($model,'createdBy', array('style'=>'width:100%','class' => 'input-text')); ?>
			<?php echo $form->error($model,'createdBy')."<br/>"; ?>
	</div>
	<div class="space10"></div>
	<div class="row75">
		<div class="label">
			<?php echo $form->labelEx($model,'Color',array('class'=>'custom-label no-width')); ?>
		</div>
		<input type=color class="color" name="CategoryCreateForm[color]"></input>
	</div>
	<div class="space20"></div>
	<div class="row75 radioBtnActive">
        <?php echo $form->labelEx($model,'activeFlag', array('class'=>'custom-label no-width active-flag')); ?>
        <?php echo $form->radioButtonList($model,'active', array('<span>Active</span>','<span>NonActive</span>'),array('separator'=>'')); ?>
    </div>
				<div class="space20"></div>
	<div class="row75">
		<div class="label">
			<?php echo $form->labelEx($model,'Parent Category',array('class'=>'custom-label'))."<br/>"; ?>
		</div>
		<select name="CategoryCreateForm[parentId]" class="parent-category">
				<option value=NULL> &nbsp </option>
				<?php
					foreach($categoryList as $a){
						echo "<option value='".$a['id']."'>".$a['category_name']."</option>";
					}
				?>
		 </select>
	</div>
	<div class="space20"></div>
	<div class="row75">
		<?php echo CHtml::submitButton('SUBMIT',array('class'=>'btnSubmitContent')); ?>
	</div>
    <?php $this->endWidget(); ?>
</div>

<script>
    var x = 0;
	
    $(document).ready(function(){
        $("#forUrl").hide();
		
        $("#klikUpload").click(function(){
            if(x == 1){
                $("#forUrl").slideUp("slow"); $("#forUpload").slideDown("slow");
				x = 0;
            }
        });
		
		$("#klikUrl").click(function(){
            if(x == 0){
                $("#forUrl").slideDown("slow"); $("#forUpload").slideUp("slow");
				x = 1;
            }
        });
    });
    </script>