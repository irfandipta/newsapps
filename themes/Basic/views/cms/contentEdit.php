<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - Edit Content';
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">

<?php if(Yii::app()->user->hasFlash('errorFlash')): ?>
    <div class="flash-error-custom">
        <?php echo Yii::app()->user->getFlash('errorFlash'); ?>
    </div>
<?php endif; ?>
    
    <?php if(Yii::app()->user->hasFlash('successFlash')): ?>
    <div class="flash-success-custom">
        <?php echo Yii::app()->user->getFlash('successFlash'); ?>
    </div>
<?php endif; ?>

<div class="contentTitleBox">
    EDIT CONTENT
                <div class="back-button"><a href="<?php echo Yii::app()->request->urlReferrer; ?>">Back</a></div>
</div>

<div class="form-createContent">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'contentedit-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array('validateOnSubmit'=>true,),
            'htmlOptions'=>array('enctype'=>'multipart/form-data',),
    )); 
    ?>
    
    <div class="row75 dropdownBoxC">
        <?php echo $form->labelEx($model,'categories', array('class'=>'custom-label')); ?>
        <?php //echo $form->dropDownList($model,'categories', $catList, array('class' => 'dropdownCategory')); ?>
        <select class="dropdownCategory" name="ContentForm[categories]" id="ContentForm_categories">
            <?php
                foreach($catList as $a){
                    $selected = "";
                    if($model['categories']==$a['id']) $selected="selected='selected'";
                    echo "<option value='".$a['id']."'".$selected.">".$a['category_name']."</option>";
                }
            ?>
        </select>
    </div>
				<div class="space10"></div>
    <div class="row75">
        <?php echo $form->labelEx($model,'title', array('class'=>'custom-label')); ?>
        <?php echo $form->textField($model,'title', array('class' => 'input-text')); ?>
    </div>
				<div class="space10"></div>
    <div class="row75">
        <?php echo $form->labelEx($model,'description', array('class'=>'custom-label')); ?>
        <?php echo $form->textArea($model,'description', array('class' => 'input-textArea')); ?>
    </div>
    <script>
    var x = 0; //upload
    var y = 0; //url
    $(document).ready(function(){
        <?php if($model['flagFiture']==2){?>
            $("#forUpload").hide(); $("#forIcon").hide(); $("#imgPrev1").hide(); y = 1; //fitur URL
        <?php } else{ ?>
            $("#forUrl").hide(); $("#forIconUrl").hide(); $("#imgPrev2").hide(); x = 1; // fitur Upload
        <?php } ?>
        $("#klikUpload").click(function(){
            $("#imgPrev2").hide();
            document.getElementById("flagFiture2").value = 1;
            if(x == 1){
                x = 0; y = 0;
                $("#forUrl").hide(); $("#forIconUrl").hide(); $("#forUpload").slideUp(); $("#forIcon").slideUp(); $("#imgPrev1").slideUp();
            }
            else{
                x = 1; y = 0;
                $("#forUrl").hide(); $("#forIconUrl").hide(); $("#forUpload").slideDown(); $("#forIcon").slideDown(); $("#imgPrev1").slideDown(); 
            }
        });
        $("#klikUrl").click(function(){
            $("#imgPrev1").hide();
            document.getElementById("flagFiture2").value = 2;
            if(y == 1){
                y = 0; x = 0;
                $("#forIcon").hide(); $("#forUpload").hide(); $("#forUrl").slideUp(); $("#forIconUrl").slideUp(); $("#imgPrev2").slideUp();
            }
            else{
                y = 1; x = 0;
                $("#forIcon").hide(); $("#forUpload").hide(); $("#forUrl").slideDown(); $("#forIconUrl").slideDown(); $("#imgPrev2").slideDown(); 
            }
        });
    });
    </script>
    <input name="ContentForm[flagFiture2]" id="flagFiture2" type="hidden" value="<?php echo $model['flagFiture'];?>"/>
    <div class="space20"></div>
    <div class="row75 custom-label">
        Content *<br/>
        <a href="#" id="klikUpload">Upload</a> |  <a href="#" id="klikUrl">URL</a>
    </div>
    <div class="row75" id="forUpload">
        <script src="<?php echo Yii::app()->baseUrl.'/ckeditor/assets/ckeditor.js'; ?>"></script>
            <?php echo $form->textArea($model, 'contentDetail', array('id'=>'textEditor')); ?>
        <script type="text/javascript">CKEDITOR.replace('textEditor');</script>
    </div>
				<div class="space20"></div>
    <div class="row75" id="forIcon">
        <?php echo $form->labelEx($model,'icon', array('class'=>'custom-label')); ?>
        <?php echo $form->fileField($model,'icon', array('class' => 'input-text')); ?>
    </div>
    <div class="row75" id="forUrl">
        <?php echo $form->labelEx($model,'url', array('class'=>'custom-label')); ?>
        <?php echo $form->textField($model,'url', array('placeholder'=>'http://www.example.com', 'class' => 'input-text')); ?>
    </div>
    <div class="row75" id="forIconUrl">
        <?php echo $form->labelEx($model,'iconUrl', array('class'=>'custom-label')); ?>
        <?php echo $form->textField($model,'iconUrl', array('class' => 'input-text', 'placeholder'=>'http://www.example.com/image.jpg')); ?>
    </div>
				<div class="space10"></div>
    <div class="row75" id="imgPrev<?php echo $model['flagFiture'];?>">
        <span class="custom-label">Previous Image Preview</span><br/>
        <img src="<?php echo $model['imagePath'];?>" alt="<?php echo $model['imagePath'];?>" title="<?php echo $model['imagePath'];?>" class="imgPrevious"/>
    </div>
    
    <div class="space20"></div>
    <div class="row75">
        <?php echo $form->labelEx($model,'publishedDate', array('class'=>'custom-label')); ?>
        <?php //echo $form->dateField($model,'publishedDate', array('class'=>'input-text', 'value'=>date('Y-m-d'))); ?>
        <script>
            $(function() {
              $( "#datepicker" ).datepicker({dateFormat:"yy-mm-dd"});
            });
        </script>
        <input type="text" id="datepicker" name="ContentForm[publishedDate]" value="<?php echo date('Y-m-d', strtotime($model['publishedDate']));?>" class="input-text" readonly/>
    </div>
    <div class="space10"></div>
    <div class="row75">
        <?php echo $form->labelEx($model,'createdBy', array('class'=>'custom-label')); ?>
        <?php echo $form->textField($model,'createdBy', array('class' => 'input-text')); ?>
    </div>
				<div class="space10"></div>
    <div class="row75 radioBtnActive">
        <?php echo $form->labelEx($model,'activeFlag', array('class'=>'custom-label no-width active-flag')); ?>
        <?php echo $form->radioButtonList($model,'activeFlag', array('<span>NonActive</span>','<span>Active</span>'),array('separator'=>'')); ?>
    </div>
    
    <div class="space20"></div>
    <div class="row75">
        <?php echo CHtml::submitButton('SUBMIT', array('class'=>'btnSubmitContent'));?>
    </div>
    <?php $this->endWidget(); ?>
</div>