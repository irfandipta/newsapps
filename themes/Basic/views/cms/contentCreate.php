<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - Create Content';
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">

<?php if(Yii::app()->user->hasFlash('errorFlash')): ?>
    <div class="flash-error-custom">
        <?php echo Yii::app()->user->getFlash('errorFlash'); ?>
    </div>
<?php endif; ?>
    
    <?php if(Yii::app()->user->hasFlash('successFlash')): ?>
    <div class="flash-success-custom">
        <?php echo Yii::app()->user->getFlash('successFlash'); ?>
    </div>
<?php endif; ?>

<div class="contentTitleBox">
    CREATE CONTENT
</div>

<div class="form-createContent">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'contentcreate-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array('validateOnSubmit'=>true,),
            'htmlOptions'=>array('enctype'=>'multipart/form-data',),
    )); 
    ?>
    
    <div class="row75 dropdownBoxC">
        <?php echo $form->labelEx($model,'categories', array('class'=>'custom-label')); ?>
        <?php //echo $form->dropDownList($model,'categories', $catList, array('class' => 'dropdownCategory')); ?>
        <select class="dropdownCategory" name="ContentForm[categories]" id="ContentForm_categories">
            <?php
                foreach($catList as $a){
                    echo "<option value='".$a['id']."'>".$a['category_name']."</option>";
                }
            ?>
        </select>
    </div>
				<div class="space10"></div>
    <div class="row75">
        <?php echo $form->labelEx($model,'title', array('class'=>'custom-label')); ?>
        <?php echo $form->textField($model,'title', array('class' => 'input-text')); ?>
    </div>
				<div class="space10"></div>
    <div class="row75">
        <?php echo $form->labelEx($model,'description', array('class'=>'custom-label')); ?>
        <?php echo $form->textArea($model,'description', array('class' => 'input-textArea')); ?>
    </div>
    <div class="space10"></div>
    <script>
    var x = 0; //upload
    var y = 0; //url
    $(document).ready(function(){
        document.getElementById("ContentForm_activeFlag_0").checked = true;
        $("#forUrl").hide(); $("#forIconUrl").hide(); $("#forUpload").hide(); $("#forIcon").hide();
        $("#klikUpload").click(function(){
            document.getElementById("flagFiture").value = 1;
            if(x == 1){
                x = 0; y = 0;
                $("#forUrl").hide(); $("#forIconUrl").hide(); $("#forUpload").slideUp(); $("#forIcon").slideUp();
            }
            else{
                x = 1; y = 0;
                $("#forUrl").hide(); $("#forIconUrl").hide(); $("#forUpload").slideDown(); $("#forIcon").slideDown();
            }
        });
        $("#klikUrl").click(function(){
            document.getElementById("flagFiture").value = 2;
            if(y == 1){
                y = 0; x = 0;
                $("#forIcon").hide(); $("#forUpload").hide(); $("#forUrl").slideUp(); $("#forIconUrl").slideUp();
            }
            else{
                y = 1; x = 0;
                $("#forIcon").hide(); $("#forUpload").hide(); $("#forUrl").slideDown(); $("#forIconUrl").slideDown();
            }
        });
    });
    </script>
				<div class="space10"></div>
    <input name="ContentForm[flagFiture]" id="flagFiture" type="hidden" value="<?php echo $model['flagFiture'];?>"/>
    
    <div class="row75 custom-label">
        Content *<br/>
        <a href="#" id="klikUpload">Upload</a> |  <a href="#" id="klikUrl">URL</a>
    </div>
    <div class="row75" id="forUpload">
        <script src="<?php echo Yii::app()->baseUrl.'/ckeditor/assets/ckeditor.js'; ?>"></script>
            <?php echo $form->textArea($model, 'contentDetail', array('id'=>'textEditor')); ?>
        <script type="text/javascript">CKEDITOR.replace('textEditor');</script>
    </div>
    <div class="row75" id="forUrl">
        <?php echo $form->labelEx($model,'url', array('class'=>'custom-label')); ?>
        <?php echo $form->textField($model,'url', array('placeholder'=>'http://www.example.com', 'class' => 'input-text')); ?>
    </div>
    <div class="row75" id="forIcon">
        <?php echo $form->labelEx($model,'icon', array('class'=>'custom-label')); ?>
        <?php echo $form->fileField($model,'icon', array('class' => 'input-text')); ?>
    </div>
    <div class="row75" id="forIconUrl">
        <?php echo $form->labelEx($model,'iconUrl', array('class'=>'custom-label')); ?>
        <?php echo $form->textField($model,'iconUrl', array('class' => 'input-text', 'placeholder'=>'http://www.example.com/image.jpg')); ?>
    </div>
    
    <div class="space20"></div>
    <div class="row75">
        <?php echo $form->labelEx($model,'publishedDate', array('class'=>'custom-label')); ?>
        <?php //echo $form->dateField($model,'publishedDate', array('class'=>'input-text', 'value'=>date('Y-m-d'))); ?>
        <script>
            $(function() {
              $( "#datepicker" ).datepicker({dateFormat:"yy-mm-dd"});
            });
        </script>
        <input type="text" id="datepicker" name="ContentForm[publishedDate]" value="<?php echo date('Y-m-d');?>" class="input-text" readonly/>
    </div>
    
				<div class="space10"></div>
                                
    <script>
    $(document).ready(function(){
        $("#other").hide();
        document.getElementById("npfield").value = document.getElementById("adminLogin").value;
        $("#passFlag1, #passFlag2").change(function () {
            if($("#passFlag1").is(":checked")){
                $("#other").hide();
                document.getElementById("npfield").value = document.getElementById("adminLogin").value;
            };
            if($("#passFlag2").is(":checked")){
                $("#other").show();
                document.getElementById("npfield").value = "";
            };
        });
    });
    </script>
    <input value="<?php echo Yii::app()->user->name;?>" type="hidden" id="adminLogin"/>
    <div class="row75">
        <?php echo $form->labelEx($model,'createdBy', array('class'=>'custom-label')); ?>
        <div>
            <input type="radio" name="createdRadio" id='passFlag1' checked='checked'/>
            <label for="passFlag1">Me</label><br/>
            <input type="radio" name="createdRadio" value=2 id='passFlag2'/>
            <label for="passFlag2">Other</label><br/>
                <span style="margin-left:40px; display:block;" id="other">
                    <?php echo $form->textField($model,'createdBy', array('class'=>'input-text','id'=>'npfield')); ?>
                </span>
        </div>
    </div>
				<div class="space10"></div>
    <div class="row75 radioBtnActive">
        <?php echo $form->labelEx($model,'activeFlag', array('class'=>'custom-label no-width active-flag')); ?>
        <?php echo $form->radioButtonList($model,'activeFlag', array('<span>NonActive</span>','<span>Active</span>'),array('separator'=>'')); ?>
    </div>
    
    <div class="space20"></div>
    <div class="row75">
        <?php echo CHtml::submitButton('SUBMIT', array('class'=>'btnSubmitContent'));?>
    </div>
    <div class="space30"></div>
    <?php $this->endWidget(); ?>
</div>