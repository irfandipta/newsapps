<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - Content List';
?>

<div class="contentTitleBox titleCList">
    <div class='titleWithSearch'>CONTENT LIST</div>
    <?php 
        $form=$this->beginWidget('CActiveForm', array(
                'id'=>'contentsearch-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array('validateOnSubmit'=>true,),
                'method'=>'get',
                'action'=>Yii::app()->createUrl('cms/ContentList'),
        )); 
    ?>
    <input type='text' class='searchBox' placeholder='Search..' name='keyword'/>
    <?php
        //echo $form->textField($model,'searchKeyword',array('placeholder'=>'Search..','class'=>'searchBox'));
        $this->endWidget(); 
    ?>
</div>

<div class="form-listContent">
    <?php 
    if(count($data) > 0){
    
    foreach($data as $a):
        $flagStatus = "Non Active";
        if($a['active_flag'] == 1) $flagStatus = "Active";
    ?>
    <div class="contentItemContainer">
        <div class="contentItemIconBox">
            <img src="<?php echo $a['icon_1'];?>" alt="<?php echo $a['icon_1'];?>" title="<?php echo $a['news_title'];?>" class="contentItemIcon"/>
        </div>
        <div class="contentItemNews">
            <div class="contentItemTitle"><?php echo $a['news_title'];?></div>
            <div class="contentItemAuthor">by <?php echo $a['created_by'];?></div>
            <div class="contentItemDesc"><?php echo $a['content_desc'];?></div>
            <div class="contentItemMisc">
                <div class="contentItemStatus"><?php echo $flagStatus;?> | <?php echo date('d F Y', strtotime($a['published_date']));?></div>
                <div class="contentItemActionBox">
                    <?php echo CHtml::link(CHtml::image(Yii::app()->theme->baseUrl."/images/edit.png"),array('EditContent','id'=>$a['id']))?>
                    &bull;&nbsp;<?php echo CHtml::link(CHtml::image(Yii::app()->theme->baseUrl."/images/delete.png"),array('DeleteContent','id'=>$a['id']),array('confirm'=>'Are you sure?'))?>
                    &bull;&nbsp;<?php echo "<a href='#' id='btn_broadcast' data-contentid = ".$a['id']." data-contenttitle = '".$a['news_title']."'>".CHtml::image(Yii::app()->theme->baseUrl."/images/push_notif.png")."</a>"; ?>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach;?>
    
    <div class="paginationBox">
        <?php 
            $this->widget('CLinkPager', array(
                'currentPage'=>$pages->getCurrentPage(),
                'itemCount'=>$item_count,
                'pageSize'=>$page_size,
                'maxButtonCount'=>10,
                'header'=>'',
                'lastPageLabel'=>'&gt;&gt;',
                'firstPageLabel'=>'&lt;&lt;',
                'nextPageLabel'=>'Next',
                'prevPageLabel'=>'Prev',
                'htmlOptions'=>array('class'=>'pagination'),
             ));
            //echo "&lt;&lt;Prev 1 2 3 4 Next&gt;&gt;";
        ?>
    </div>
				<div class="space20"></div>
    <?php } else{ ?>
        <div class="contentNotFound">
            Not Found
        </div>
    <?php } ?>
</div>

<script>
	$("#btn_broadcast").click(function(){
		var contentId = $(this).data("contentid");
		var contentTitle = $(this).data("contenttitle");
		$.ajax({
                    url: "<?php echo $url['ajax_broadcast']; ?>",
                    type: 'POST',
                    data: {'contentId' : contentId , 'contentTitle' : contentTitle},
                    success: function(data) {
                        alert(data);
                    },
                    error: function(e) {
                         console.log(e.message);
                    }
                });
	})
</script>