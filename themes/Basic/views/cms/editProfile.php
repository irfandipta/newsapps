<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name. " - Edit Profile";
?>

<script>
    $(document).ready(function(){
        $("#newPassBox").hide();
        $("#passFlag1, #passFlag2").change(function () {
            if($("#passFlag1").is(":checked")){
                $("#newPassBox").hide();
                document.getElementById("npfield").value = "";
                document.getElementById("cpfield").value = "";
            };
            if($("#passFlag2").is(":checked")){
                $("#newPassBox").show();
            };
        });
    });
    
</script>

<?php if(Yii::app()->user->hasFlash('errorFlash')): ?>
    <div class="flash-error-custom">
        <?php 
            $error = Yii::app()->user->getFlash('errorFlash');
            echo $error; 
        ?>
    </div>
<?php endif; ?>
    
    <?php if(Yii::app()->user->hasFlash('successFlash')): ?>
    <div class="flash-success-custom">
        <?php echo Yii::app()->user->getFlash('successFlash'); ?>
    </div>
<?php endif; ?>

<?php
    if($error == 0){
?>
<div class="contentTitleBox">
    EDIT PROFILE - <?php echo strtoupper($model->username);?>
				<div class="back-button"><a href="<?php echo Yii::app()->request->urlReferrer; ?>">Back</a></div>
</div>

<div class="form-createContent">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'editProfile-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array('validateOnSubmit'=>true,),
    )); 
    ?>
    <div class="row75">
        <label class="custom-label">Username</label>
        <?php echo $form->textField($model,'username', array('class'=>'input-text')); ?>
    </div>
				<div class="space10"></div>
    <div class="row75">
        <label class="custom-label">Email</label>
        <?php echo $form->textField($model,'email', array('class'=>'input-text')); ?>
    </div>
				<div class="space10"></div>
    <div class="row75">
        <label class="custom-label">Password</label>
        <div>
            <input type="radio" name="UserForm[passFlag]" value=1 id='passFlag1' checked='checked'/>
            <label for="passFlag1">Use Old Password</label><br/>
            <input type="radio" name="UserForm[passFlag]" value=2 id='passFlag2'/>
            <label for="passFlag2">Insert New Password</label><br/>
                <span style="margin-left:40px; display:block;" id="newPassBox">
                    New Password : <br/>
                    <?php echo $form->passwordField($model,'newPassword', array('class'=>'input-text','id'=>'npfield')); ?>
                    Confirm Password : <br/>
                    <?php echo $form->passwordField($model,'confNewPassword', array('class'=>'input-text','id'=>'cpfield')); ?>
                </span>
        </div>
    </div>
				<div class="space20"></div>
				<?php if(Yii::app()->user->superuser == 1): ?>
    <div class="row75">
        <label class="custom-label">Status</label>
        <div>
            <input type="radio" name="UserForm[status]" value=1 id='statFlag1' <?php if($model->status==1) echo "checked=''checked"?>/>
            <label for="statFlag1">Active</label><br/>
            <input type="radio" name="UserForm[status]" value=0 id='statFlag2' <?php if($model->status==0) echo "checked=''checked"?>/>
            <label for="statFlag2">NonActive</label><br/>
        </div>
    </div>
				<?php endif;?>
				<div class="space20"></div>
    <div class="row75">
        <label class="custom-label">Super User</label>
        <div>
            <input type="radio" name="UserForm[superuser]" value=1 id='superFlag1' <?php if($model->superuser==1) echo "checked=''checked"?>/>
            <label for="superFlag1">Yes</label><br/>
            <input type="radio" name="UserForm[superuser]" value=0 id='superFlag2' <?php if($model->superuser==0) echo "checked=''checked"?>/>
            <label for="superFlag2">No</label><br/>
        </div>
    </div>
    <div class="space20"></div>
    <div class="row75">
        <?php echo CHtml::submitButton('Submit', array('class'=>'btnSubmitContent'));?>
    </div>
    
    <?php $this->endWidget();?>
</div>
<?php }
else {
?>
<div class="flash-error-custom">
    ERROR!!! This ID not valid.
</div>
<?php } ?>