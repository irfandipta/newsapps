<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - Content List';
?>

<div class="categoryTitleBox titleCList">
    <div class='title_only'>CATEGORY LIST</div>
</div>

<div class="form-listContent" onresize="resize()">
    <?php 
	$arrCat = array();
	$idxCat = 0;
	$idx1 = 0;
	$child = 0;
	$arrCountCat = array();
	
	$arrSubCat = array();
	
	foreach($data as $x)
	{
		$arrCat[$idxCat]['sort_code'] = $x['sort_code'];
		$arrCat[$idxCat]['id'] = $x['id'];
		
		$arrCountCat[$idxCat] = 0;
		$idxSubCat = 0;
		
		foreach($subdata as $y)
		{
			if($x['id'] == $y['parentId'])
			{
				$arrSubCat[$idxCat][$idxSubCat]['id'] = $y['id'];
				$arrSubCat[$idxCat][$idxSubCat]['sort_code'] = $y['sort_code'];
				$arrCountCat[$idxCat] = $arrCountCat[$idxCat]+1;
				$idxSubCat ++;
			}
		}
		
		$idxCat++;
	}
    if(count($data) > 0){
    $inc = 0;
    foreach($data as $a):
		
		$child = 0;
        $flagStatus = "Active";
        if($a['active_flag'] == 1) $flagStatus = "Non Active";
		
		
		foreach($subdata as $y)
		{
			if($a['id'] == $y['parentId']) $child++;
		}
   ?>
    <div class="categoryItemContainer" id="main_cat" style="border:1px solid #ebebeb; background: #fefefe;">
        <div class="categorynaviconbox">
			<div class="up">
				<?php
					$tag = CHtml::tag('div',array('class'=>'up'),'');
					if($idx1>0) echo CHtml::link($tag, array('cms/SwapSortCode','id1'=>$arrCat[$idx1]['id'], 'sort1'=>$arrCat[$idx1]['sort_code'], 'id2'=>$arrCat[$idx1-1]['id'], 'sort2'=>$arrCat[$idx1-1]['sort_code']));
					else {
							$tag = CHtml::tag('div',array('class'=>'up_final'),'');
							echo $tag;
						}
				?>
			</div>
			<div class="down">
				<?php
					
					$tag = CHtml::tag('div',array('class'=>'down'),'');
					if($idx1<$idxCat-1)echo CHtml::link($tag, array('cms/SwapSortCode','id1'=>$arrCat[$idx1]['id'], 'sort1'=>$arrCat[$idx1]['sort_code'], 'id2'=>$arrCat[$idx1+1]['id'], 'sort2'=>$arrCat[$idx1+1]['sort_code']));
					else {
						$tag = CHtml::tag('div',array('class'=>'down_final'),'');
						echo $tag;
					}
				?>
			</div>
		</div>
		<div class="categoryItemIconBox">
            <img src="<?php echo $a['icon_1'];?>" alt="<?php echo $a['icon_1'];?>" class="contentItemIcon"/>
        </div>
        <div class="categoryItemNews">
            <div class="contentItemTitle"><?php echo $a['category_name'];?></div>
            <div class="contentItemAuthor">by <?php echo $a['created_by'];?></div>
            <div class="contentItemDesc"></div>
            <div class="contentItemMisc">
                <div class="contentItemStatus"><?php echo $flagStatus;?> | <a class="show" id="disp<?php echo $inc;?>" onclick="disp_sub(<?php echo $inc?>,<?php echo $child?>)" style="cursor: pointer; cursor: hand;">Sub Category(<?php echo $child?>)</a></div>
                <div class="categoryItemActionBox"><?php echo CHtml::link(CHtml::image(Yii::app()->theme->baseUrl."/images/edit.png"), array('cms/CategoryEdit','id'=>$a['id']));?> | <?php echo CHtml::link(CHtml::image(Yii::app()->theme->baseUrl."/images/delete.png"), array('cms/CategoryDelete', 'id'=>$a['id']), array('confirm'=>'Are you sure?'));?></div>
            </div>
        </div>
		<div id="color_box" style="background-color: <?php echo $a['color_code']; ?>">
		</div>
    </div>
	<div class="sub_cat" id="sub<?php echo $inc?>" style="max-height:0;  overflow:hidden;">
	<?php
		$idx2 = 0;
		foreach($subdata as $y)
		{
			if($a['id'] == $y['parentId'])
			{
			?>
				<div class="categoryItemContainer sub" id="subcat" style="border:1px solid #ebebeb; background: #fefefe;">
					<div class="categorynaviconbox">
						<div class="up">
							<?php
								
								$tag = CHtml::tag('div',array('class'=>'up'),'');
								if($idx2>0) echo CHtml::link($tag, array('cms/SwapSortCode','id1'=>$arrSubCat[$idx1][$idx2]['id'], 'sort1'=>$arrSubCat[$idx1][$idx2]['sort_code'], 'id2'=>$arrSubCat[$idx1][$idx2-1]['id'], 'sort2'=>$arrSubCat[$idx1][$idx2-1]['sort_code']));
								else {
									$tag = CHtml::tag('div',array('class'=>'up_final'),'');
									echo $tag;
								}
							?>
						</div>
						<div class="down">
							<?php
								$tag = CHtml::tag('div',array('class'=>'down'),'');
								if($idx2<$arrCountCat[$idx1]-1) echo CHtml::link($tag, array('cms/SwapSortCode','id1'=>$arrSubCat[$idx1][$idx2]['id'], 'sort1'=>$arrSubCat[$idx1][$idx2]['sort_code'], 'id2'=>$arrSubCat[$idx1][$idx2+1]['id'], 'sort2'=>$arrSubCat[$idx1][$idx2+1]['sort_code']));
								else {
									$tag = CHtml::tag('div',array('class'=>'down_final'),'');
									echo $tag;
								}
							?>
						</div>
					</div>
				<div class="categoryItemIconBox">
						<img src="<?php echo $y['icon_1'];?>" alt="<?php echo $y['icon_1'];?>" class="contentItemIcon"/>
					</div>
					<div class="categoryItemNews">
						<div class="contentItemTitle"><?php echo $y['category_name'];?></div>
						<div class="contentItemAuthor">by <?php echo $y['created_by'];?></div>
						<div class="contentItemDesc"></div>
						<div class="contentItemMisc">
							<div class="contentItemStatus" style="height:10px"></div>
							<div class="categoryItemActionBox"><?php echo CHtml::link(CHtml::image(Yii::app()->theme->baseUrl."/images/edit.png"), array('cms/CategoryEdit','id'=>$y['id']));?> | <?php echo CHtml::link(CHtml::image(Yii::app()->theme->baseUrl."/images/delete.png"), array('cms/CategoryDelete', 'id'=>$y['id']), array('confirm'=>'Are you sure?'));?></div>
						</div>
					</div>
					
					<div id="color_box" style="width: 28.8px; background-color: <?php echo $y['color_code']; ?>">
					</div>
				</div>
			<?php
				$idx2++;
			}
		}
		$inc++;
		$idx1++;
	?>
    </div>
    <?php endforeach;
	?>
	
   
    <?php } else{ ?>
        <div class="contentNotFound">
            Not Found
        </div>
    <?php } ?>
</div>