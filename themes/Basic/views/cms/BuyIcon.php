<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - Content List';
?>

<div class="categoryTitleBox titleCList" style="font-size: 17px;">
    <div class='only_title' >Buy Icon</div>
	<div class='only_back'><?php echo CHtml::link('Back',array('ApplicationIcon')); ?></div>
</div>

<div class="iconContainer">
<?php
	foreach($data as $x)
	{
		?>
		<div class='iconBoxContainer general_background' >
			<div class='iconBox' style="background-color:<?php echo $x['color']?>"></div>
			<div class='iconDetail' style="width:65%; margin-top:10px; margin-left:10px">Rp.<?php echo  $x['price'] ?>,00</div>
			<div class='iconBuy' style="width:25%; margin-top:10px">
				<?php
					$imghtml=CHtml::image(Yii::app()->theme->baseUrl.'//css//img//ico_buy.png','ico_buy.png',array('width'=>'15px','height'=>'15px'));
					echo CHtml::link($imghtml,array('BuyConf','id'=>$x['id']));
				?>
			</div>
		</div>
		<?php
	}
?>
</div>
<script>

	$(".iconBuy").hover(function () {
		$(this).closest(".iconBoxContainer").toggleClass("hovered");
	});
</script>
	
   