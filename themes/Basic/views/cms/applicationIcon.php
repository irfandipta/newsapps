<?php
/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name. " - Application Icon";
?>

<?php if(Yii::app()->user->hasFlash('errorFlash')): ?>
    <div class="flash-error-custom">
        <?php echo Yii::app()->user->getFlash('errorFlash'); ?>
    </div>
	<div class="space20"></div>
<?php endif; ?>
    
<?php if(Yii::app()->user->hasFlash('successFlash')): ?>
    <div class="flash-success-custom">
        <?php echo Yii::app()->user->getFlash('successFlash'); ?>
    </div>
	<div class="space20"></div>
<?php endif; ?>

<div class="contentTitleBox">
    <div class='titleWithSearch'>Application Icon</div>
    <div class='searchBox'>
        <?php 
            $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'buyicon-form',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array('validateOnSubmit'=>true,),
                    'action'=>Yii::app()->createUrl('cms/BuyIcon'),
            )); 
        ?>
        <button><i><img src="<?php echo Yii::app()->theme->baseUrl;?>/css/img/ico_buy.png" style="width:15px;height:15px;"/></i> Buy Icon</button>
        <?php
            $this->endWidget(); 
        ?>
    </div>
</div>

<div class="form-listContent">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'applicationIcon-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array('validateOnSubmit'=>true,),
            'htmlOptions'=>array('enctype'=>'multipart/form-data',),
    )); 
    ?>
	
	<div class="row75">
        <span class="label-custom-app">Application Name:</span>
        <?php echo $form->textField($model,'appName', array('class'=>'input-text')); ?>
    </div>
	
	<div class="space10"></div>
	<div class="row75">
        <span class="label-custom-app">Application Short Name:</span>
        <?php echo $form->textField($model,'appShortName', array('class'=>'input-text')); ?>
    </div>
    
	<div class="space10"></div>
    <div class="row75 label-custom-app">
        Pick Themes: 
    </div>
    <div class="row75" style="background-color:white; overflow:auto; border:3px solid #ebebeb; height: 90px;">
        <div class="radio-custom-theme">
            <?php foreach($themeList as $a): ?>
                <input type="radio" name="ThemeForm[templateID]" value="<?php echo $a['id'];?>" id="themeradio<?php echo $a['id'];?>" <?php if($a['id']==$choosenTemp) echo "checked='checked'"?>/>
                <label for="themeradio<?php echo $a['id'];?>"><span class="radioTheme" style="background-color:<?php echo $a['color'];?>;" title='<?php echo $a['name'];?>'></span></label>
            <?php endforeach;?>
        </div>
    </div>
	
    <div class="space10"></div>
    <div class="row75">
        <span class="label-custom-app">Pick Logo:</span>
        <?php echo $form->textField($model,'prevIcon', array('class'=>'input-text', 'hidden'=>'hidden')); ?>
        <?php echo $form->fileField($model,'icon'); ?>
    </div>
	
	<div class="space10"></div>
	<div class="row75">
        <span class="label-custom-app">Pick Splash Screen:</span>
        <?php echo $form->textField($model,'prevSplashScreen', array('class'=>'input-text', 'hidden'=>'hidden')); ?>
        <?php echo $form->fileField($model,'splashScreen'); ?>
    </div>
	
	<div class="space10"></div>
    <div class="row75">
	
    <?php if($model->prevIcon != "" && $model->prevIcon != null):?>
	<div style="width:50%; float:left;">
        <span class="label-custom-app">Previous Logo Preview:<br/></span>
        <img src="<?php echo $model->prevIcon;?>" style="width:200px;height:200px; border:1px solid #ebebeb;" alt="<?php echo $model->prevIcon;?>" title="Logo"/>
    </div>
	<?php endif;?>
	
	<?php if($model->prevSplashScreen != "" && $model->prevSplashScreen != null):?>
    <div style="width:50%; float:left;">
        <span class="label-custom-app">Previous Splash Screen Preview:<br/></span>
        <img src="<?php echo $model->prevSplashScreen;?>" style="width:200px;height:200px; border:1px solid #ebebeb;" alt="<?php echo $model->prevSplashScreen;?>" title="Splash Screen"/>
    </div>
    <?php endif;?>
	</div>
	
    <div class="space20"></div>
    <div class="row75">
        <?php echo CHtml::submitButton('Submit', array('class'=>'btnSubmitContent'));?>
    </div>
    <div class="space20"></div>
    <?php $this->endWidget();?>
</div>