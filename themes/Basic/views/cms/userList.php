<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name. " - User List";
?>

<div class="contentTitleBox">
    USER LIST
</div>

<div class="form-createContent">
    <?php foreach($list as $a): ?>
    <div class="contentItemContainer general_background" style="padding: 2px; width: 45%;">
        <div style="width:80%; float:left;">
            <table>
                <tr>
                    <td style="width:25%">Username</td>
                    <td style="width:5%">:</td>
                    <td style="width:70%"><?php echo "<b>".strtoupper($a['username'])."</b>";?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td><?php echo $a['email'];?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>:</td>
                    <td><?php if($a['status']==1) echo "Active"; else echo "Not Active";?></td>
                </tr>
                <tr>
                    <td>Super User</td>
                    <td>:</td>
                    <td><?php if($a['superuser']==1) echo "Yes"; else echo "No";?></td>
                </tr>
                <tr>
                    <td>Created Date</td>
                    <td>:</td>
                    <td><?php  echo $a['create_at'];?></td>
                </tr>
                <tr>
                    <td>Last Visit</td>
                    <td>:</td>
                    <td><?php if($a['lastvisit_at']=="0000-00-00 00:00:00") echo "-"; else echo $a['lastvisit_at'];?></td>
                </tr>
            </table>
        </div>
        <div style="float:left; width: 20%; font-size:25px;">
            <br/>
            <?php 
                if(($a['superuser'] == 1 && $a['username'] == Yii::app()->user->name) || $a['superuser'] == 0){
                    $editImg = Yii::app()->theme->baseUrl.'/images/edit.png';
                    echo CHtml::link('<img src="'.$editImg.'" alt="Edit Profile" title="Edit Profile" style="margin:5px 5px 0px 5px; width:20x; height:20px;"/>', array('EditProfile','id'=>$a['id']));
                }
			?>
			<?php
				if($a['username'] != Yii::app()->user->name && ($a['superuser'] == 1 && $a['username'] == Yii::app()->user->name) || $a['superuser'] == 0) echo "<b>|</b>";
                if($a['username'] != Yii::app()->user->name){
                    $delImg = Yii::app()->theme->baseUrl.'/images/delete.png';
                    echo   CHtml::link('<img src="'.$delImg.'" alt="Delete Account" title="Delete Account" style=" margin:5px 5px 0px 5px; width:20px; height:20px;"/>', array('DeleteAccount', 'id'=>$a['id']), 
                            array('confirm'=>'Are you sure to delete user '.strtoupper($a['username']).'?'));
                }
            ?>
        </div>
    </div>
    <?php endforeach;?>
</div>