<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - Reminder';
?>

<div class="form-createContent">
	<?php if(count($comment) > 0){
		$i = 1;
		foreach($comment as $c):
			$i++;
	?>
	
		<div class="commentItemBox" style="border-bottom:1px solid #ccc; width:98%; float:left; margin-bottom:5px;">
			<div class="reminderFrom">
			<?php 
				if($c['fromName'] == Yii::app()->user->name) {echo '<b>Me</b>';}
				if($c['fromName'] != Yii::app()->user->name) {echo '<b>'.$c['fromName'].'</b>';}
				$thisDate = date('Y-m-d', strtotime($c['created']));
				echo '<span class="reminderDate">';
				if($thisDate == date('Y-m-d')) echo 'Today at '.date('H:i:s', strtotime($c['created']));
				else if($thisDate == date('Y-m-d', strtotime('-1 day'))) echo 'Yesterday at '.date('H:i:s', strtotime($c['created']));
				else echo date('d F Y H:i:s', strtotime($c['created']));
				echo '</span>';
			?>
			</div>
			<div class="reminderMessage" style="font-size:12px;"><?php echo $c['message'];?></div>
			<?php if($c['read'] == 1 && $i == count($comment)+1 && $c['toName']!=Yii::app()->user->name):?>
			<span class="reminderDate">Read by <?php echo $c['toName'];?></span>
			<?php endif;?>
		</div>
		
	<?php endforeach; } else { ?>
		<div class="noComment">No Comment</div>
	<?php }?>
</div>