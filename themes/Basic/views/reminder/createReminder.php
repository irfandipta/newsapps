<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - Create Reminder';
?>

<div style='padding:10px; background:#fff; height: 260px;'>

	<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'remindercreate-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array('validateOnSubmit'=>true,),
	)); 
	?>
	
	<div class="contentTitleBox" style="font-size:16px;">Create Reminder</div>
	<div class="form-createContent">
		<div class="row100">
			<label class="labelPopUp">To :</label>
			<select class="dropdownPopUp" name="Reminder[toID]">
				<!--<option value="0">-- Select User --</option>-->
				<?php foreach($userList as $u):	?>
					<option value="<?php echo $u['id'];?>"><?php echo $u['username']; if($u['superuser'] == 1){echo " (Super User)";}?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="row100">
			<label class="labelPopUp">Message :</label>
			<?php echo $form->textArea($model,'message', array('class' => 'textareaPopUp')); ?>
		</div>
		<div class="row100 btnBoxPopUp">
			<div class="btnExPopUp"> <div style="float:right;"> <?php echo CHtml::submitButton('Save', array('class'=>'btnPopUp')); ?> </div> </div>
			<div class="btnExPopUp"> <button onclick="parent.$.colorbox.close(); return false;" class="btnPopUp">Cancel</button> </div>			
		</div>
	</div>
	
	<?php $this->endWidget(); ?>
</div>