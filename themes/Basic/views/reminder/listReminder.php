<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - Reminder';
?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ; ?>/js/jquery.colorbox.js" ></script>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ; ?>/css/colorbox.css">
<script>
	$(document).ready(function(){
		$(".popUp").colorbox();
	});
	
	function loadComment(id){
		$.ajax({
			url: '<?php echo Yii::app()->request->hostInfo.Yii::app()->baseUrl;?>?r=reminder/comment&parentId='+id,
			type: 'GET',
			beforeSend: function(){$('#commList'+id).addClass("loadingBox");},
			complete: function(){$('#commList'+id).removeClass("loadingBox");},
			success: function(result) {	$('#commList'+id).html(result); $('#coL'+id).scrollTop($('#coL'+id)[0].scrollHeight);},
			error: function(e) { console.log(e.message); }
		});
	}
	
	function readStat(id, from){
		$.ajax({
			url: '<?php echo Yii::app()->request->hostInfo.Yii::app()->baseUrl;?>?r=reminder/read&parentId='+id+'&from='+from,
			type: 'GET',
			success: function(result) {	},
			error: function(e) { console.log(e.message); }
		});
	}
	
	$(document).on('click','#ico-comm',function() { //tekan tombol comment
		var id = $(this).data('taskid');
		var from = $(this).data('fromm');
		//show comment box
		if($(this).hasClass("ico-comm-down")){ //hide -> show
			//ubah read status
			readStat(id, from);
			//tampilan comment box
			$("div.re-arrow").removeClass("ico-comm-up").addClass("ico-comm-down"); $("div.re-arrow").attr('title', 'Show Comment');
			$(this).removeClass("ico-comm-down").addClass("ico-comm-up"); $(this).attr('title', 'Hide Comment');
			$(".reminderBoxComment").height(0);	$(".reminderBoxComment").css("visibility", 'hidden');
			$("#rc"+id).height(350); $("#rc"+id).css("visibility", 'visible');
			//load comment ajax
			loadComment(id);
		}
		else if($(this).hasClass("ico-comm-up")){ //show -> hide
			$("div.re-arrow").removeClass("ico-comm-up").addClass("ico-comm-down"); $("div.re-arrow").attr('title', 'Show Comment');
			$(".reminderBoxComment").height(0);	$(".reminderBoxComment").css("visibility", 'hidden');
		}
	});
	
	$(document).on('change','#chkTask',function(){ //ubah checkbox
		var taskId = $(this).data('taskid');
		if($(this).is(":checked")) { 
			$(this).attr('title', 'Reminder Status: Done');
			$.ajax({
				url: '<?php echo Yii::app()->request->hostInfo.Yii::app()->baseUrl;?>?r=reminder/done&id='+taskId+'&done=1',
				type: 'GET',
				success: function(result) {	 },
				error: function(e) { console.log(e.message); }
			});
        } 
		else {
			$(this).attr('title', 'Reminder Status: Undone');
			$.ajax({
				url: '<?php echo Yii::app()->request->hostInfo.Yii::app()->baseUrl;?>?r=reminder/done&id='+taskId+'&done=0',
				type: 'GET',
				success: function(result) {	 },
				error: function(e) { console.log(e.message); }
			});
		}
	});
	
	$(document).on('click','#ico-del',function() { //tekan tombol delete
		var taskId = $(this).data('taskid');
		var conf = confirm("Are you sure to delete this reminder?"); //alert confirm
		if (conf) {
			$.ajax({
				url: '<?php echo Yii::app()->request->hostInfo.Yii::app()->baseUrl;?>?r=reminder/delete&taskid='+taskId,
				type: 'GET',
				success: function(result) {	window.location.reload(true); },
				error: function(e) { console.log(e.message); }
			});
		} else { }
	});
	
	$(document).on('click','#postComm',function(){ //kirim comment
		var parentId = $(this).data('parent');
		var toId = $(this).data('touser');
		var msg = $('#commMsg'+parentId).val().replace(/\r\n|\r|\n/g,"<br/>");
		$.ajax({
			url: '<?php echo Yii::app()->request->hostInfo.Yii::app()->baseUrl;?>?r=reminder/addComment&parentId='+parentId+'&to='+toId+'&msg='+msg,
			type: 'GET',
			success: function(data) { $('#commMsg'+parentId).val(null); loadComment(parentId)},
			error: function(e) { console.log(e.message); }
		});
	});
	<?php if($id != 0):?>
	loadComment(<?php echo $id;?>);
	<?php endif;?>
</script>

<div class="contentTitleBox">
    <div class="only_title">REMINDER</div>
	<div class="only_edit_profile" style="float: right;">
		<button class='popUp' href="<?php echo Yii::app()->request->hostInfo.Yii::app()->baseUrl;?>/?r=reminder/create">
			<i><img src="<?php echo Yii::app()->theme->baseUrl;?>/css/img/ico-reminder.png" style="width:15px;height:15px;"/></i> Create Reminder
		</button>
	</div>
</div>

<div class="form-createContent">
	<?php 
	if(count($reminder) > 0){
		foreach($reminder as $r):
	?>
		<div class="reminderBox">
			<div class="reminderBoxHeader">
				<div class="reminderBoxHeaderLeft">
					<div class="reminderFrom"><?php 
						if($r['fromName'] == Yii::app()->user->name) {echo "To <b>".$r['toName']."</b>"; }
						if($r['toName'] == Yii::app()->user->name) {echo "From <b>".$r['fromName']."</b>"; }
						$thisDate = date('Y-m-d', strtotime($r['created']));
						echo '<span class="reminderDate">';
						if($thisDate == date('Y-m-d')) echo 'Today at '.date('H:i:s', strtotime($r['created']));
						else if($thisDate == date('Y-m-d', strtotime('-1 day'))) echo 'Yesterday at '.date('H:i:s', strtotime($r['created']));
						else echo date('d F Y H:i:s', strtotime($r['created']));
						echo '</span>';
						?>
					</div>
					<div class="reminderMessage"><?php echo $r['message'];?></div>
				</div>
				<div class="reminderBoxHeaderRight">
					<div class="reminder-icoBox"><div class="reminder-ico re-arrow <?php if($id==$r['id']) echo 'ico-comm-up'; else echo 'ico-comm-down';?>" title="Show Comment" id="ico-comm" data-taskid="<?php echo $r['id']?>" data-fromm="<?php echo $r['from'];?>"></div></div>					
					<div class="reminder-icoBox"><div class="reminder-ico" title="Delete Reminder" id="ico-del" data-taskid="<?php echo $r['id']?>"></div></div>
					<div class="reminder-icoBox"><input type="checkbox" name="taskCheck" title="Reminder Status: <?php if($r['done']==0) echo 'Undone'; else echo 'Done'?>" id="chkTask" data-taskid="<?php echo $r['id']?>" class="reminder-check" <?php if($r['done']==1) echo "checked='checked'"?> /></div>
				</div>
			</div>
			
			<div class="reminderBoxComment" id="rc<?php echo $r['id']?>" <?php if($id==$r['id']) echo "style='height:350px; visibility:visible;'";?>>
				<div class="reminderCommentList" id="coL<?php echo $r['id'];?>"><div id="commList<?php echo $r['id'];?>"></div>
				</div>
				<div class="reminderCommentForm">
					<div class="row100"><textarea placeholder="Write a Comment..." class="reminder-commentTextArea" id="commMsg<?php echo $r['id'];?>"></textarea></div>
					<div class="row100"><button class="reminder-commentBtn" id="postComm" data-parent="<?php echo $r['id'];?>" data-touser="<?php if($r['fromName'] == Yii::app()->user->name) {echo $r['to'];} if($r['toName'] == Yii::app()->user->name) {echo $r['from'];}?>">Post</button></div>
				</div>
			</div>
		</div>
	<?php endforeach;
	}
	else { ?>
		<div class="noReminder">No Reminder</div>
	<?php } ?>
</div>