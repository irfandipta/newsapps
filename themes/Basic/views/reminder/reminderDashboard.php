<?php

/* 
 * Author : Veronica Mutiana
 */
$this->pageTitle=Yii::app()->name . ' - Reminder';
?>

	<?php 
	if(count($db) > 0){
		foreach($db as $r):
	?>
		<div class="reminderBox" style="width:98%; margin:0px; border:none; border-bottom:2px solid #ccc; border-radius:0px;">
			<div class="row100"><?php 
				if($r['fromName'] == Yii::app()->user->name) {echo "To <b>".$r['toName']."</b>"; }
				if($r['toName'] == Yii::app()->user->name) {echo "From <b>".$r['fromName']."</b>"; }
				$thisDate = date('Y-m-d', strtotime($r['created']));
				?>
				<span class="reminderDate">
				<?php
					if($thisDate == date('Y-m-d')) echo 'Today at '.date('H:i:s', strtotime($r['created']));
					else if($thisDate == date('Y-m-d', strtotime('-1 day'))) echo 'Yesterday at '.date('H:i:s', strtotime($r['created']));
					else echo date('d F Y H:i:s', strtotime($r['created']));
				?>
				</span>
			</div>
			<div class="row100"><?php echo $r['message'];?><br/>&nbsp;&gt;&nbsp;<a href="?r=reminder/list&id=<?php echo $r['id'];?>">Read Comment</a></div>
		</div>
		
	<?php endforeach;
	}
	else { ?>
		<div class="noReminder">No Reminder</div>
	<?php } ?>
