<?php /* @var $this Controller */ 
//if(!isset(Yii::app()->user->superuser)) $this->redirect(Yii::app()->user->returnUrl.'?r=site/login');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl ; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl ; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl ; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl ; ?>/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl ; ?>/css/form.css" />
        
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
        
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" ></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ; ?>/js/base.js" ></script>
		<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ; ?>/js/accordion.js" ></script>
</head>

<body>
    <div class="header">
        <div class="logo"></div>
        <div class="menuContainer">
            <div class="menuLayout menuText">Welcome, <?php echo strtoupper(Yii::app()->user->name);?></div>
			<div class="menuLayout accountMenu">
				<a href="index.php"><img src="<?php echo Yii::app()->theme->baseUrl ; ?>/css/img/ico_dashboard.png" class="accountImg" title="Dashboard"/></a>
			</div>
            <div class="menuLayout accountMenu">
                <img src="<?php echo Yii::app()->theme->baseUrl ; ?>/css/img/ico_user_action.png" class="accountImg" title="Account"/>
                <div class="accSubMenuContainer">
                    <div class="accSubMenu"><a href="?r=cms/profile">Profile</a></div>
					<div class="accSubMenu"><a href="?r=reminder/list">Reminder</a></div>
                    <div class="accSubMenu"><a href="?r=site/logout">Logout</a></div>
                </div>
            </div>
        </div>
    </div><!-- header -->

	<div class="mainmenu">
            <div class="mainmenu-nav">
                <div class="mainmenu-items">
                    <div class="mainmenu-icons" id="icon-report"></div>
                    <div class="mainmenu-labels">Report</div>
                    <div class="mainmenu-expand" data-id="6" data-action="expand">+</div>
                </div>
                <div class="submenu" id="submenu-6">
                    <div class="submenu-items"><a href="?r=report/statistic1">User Statistic</a></div>
					<div class="submenu-items"><a href="?r=report/statistic2">Android &amp; iOS User Statistic</a></div>
					<div class="submenu-items"><a href="?r=report/statistic3">Compare Android &amp; iOS User Statistic</a></div>
                </div>
            </div>
			<div class="mainmenu-nav">
                <div class="mainmenu-items">
                    <div class="mainmenu-icons" id="icon-content"></div>
                    <div class="mainmenu-labels">Content</div>
                    <div class="mainmenu-expand" data-id="1" data-action="expand">+</div>
                </div>
                <div class="submenu" id="submenu-1">
                    <div class="submenu-items"><a href="?r=cms/contentList">List</a></div>
                    <div class="submenu-items"><a href="?r=cms/contentCreate">Create</a></div>
                </div>
            </div>
            <div class="mainmenu-nav">
                <div class="mainmenu-items">
                    <div class="mainmenu-icons" id="icon-category"></div>
                    <div class="mainmenu-labels">Category</div>
                    <div class="mainmenu-expand" data-id="2" data-action="expand">+</div>
                </div>
                <div class="submenu" id="submenu-2">
                    <div class="submenu-items"><a href="?r=cms/CategoryList">List</a></div>
                    <div class="submenu-items"><a href="?r=cms/CategoryCreate">Create</a></div>
                </div>
            </div>
            
			<?php if(isset(Yii::app()->user->superuser) && Yii::app()->user->superuser == 1):?>
            <div class="mainmenu-nav">
                <div class="mainmenu-items">
                    <div class="mainmenu-icons" id="icon-appsmaker"></div>
                    <div class="mainmenu-labels">Apps Builder</div>
                    <div class="mainmenu-expand" data-id="3" data-action="expand">+</div>
                </div>
                <div class="submenu" id="submenu-3">
                    <div class="submenu-items"><a href="?r=cms/applicationIcon">Application Icon</a></div>
                    <div class="submenu-items"><a href="#">Generate</a></div>
                </div>
            </div>
			<?php endif;?>
			<!--
            <div class="mainmenu-nav">
                <div class="mainmenu-items">
                    <div class="mainmenu-icons" id="icon-broadcast"></div>
                    <div class="mainmenu-labels">Broadcast</div>
                    <div class="mainmenu-expand" data-id="4" data-action="expand">+</div>
                </div>
                <div class="submenu" id="submenu-4">
                    <div class="submenu-items"><a href="#">Push Notification</a></div>
                </div>
            </div> -->
            <?php if(isset(Yii::app()->user->superuser) && Yii::app()->user->superuser == 1):?>
            <div class="mainmenu-nav">
                <div class="mainmenu-items">
                    <div class="mainmenu-icons" id="icon-user"></div>
                    <div class="mainmenu-labels">User</div>
                    <div class="mainmenu-expand" data-id="5" data-action="expand">+</div>
                </div>
                <div class="submenu" id="submenu-5">
                    <div class="submenu-items"><a href="?r=cms/userList">List</a></div>
                    <div class="submenu-items"><a href="?r=cms/userCreate">Create</a></div>
                </div>
            </div>
            <?php endif;?>
		<?php /* $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); */ ?>
	</div><!-- mainmenu -->
	<?php  /*if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif */?>
        <div class="content">
            <div class="container">
                <?php echo $content; ?>
            </div>
        </div>
	
        <!--
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div> footer -->



</body>
</html>
