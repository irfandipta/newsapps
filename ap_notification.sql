-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 20, 2014 at 10:54 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `newsapps`
--

-- --------------------------------------------------------

--
-- Table structure for table `ap_notification`
--

CREATE TABLE IF NOT EXISTS `ap_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_id` varchar(250) NOT NULL,
  `content_id` int(11) NOT NULL,
  `response` int(1) NOT NULL,
  `response_code` int(11) NOT NULL,
  `response_message` text NOT NULL,
  `platform` varchar(16) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `ap_notification`
--

INSERT INTO `ap_notification` (`id`, `reg_id`, `content_id`, `response`, `response_code`, `response_message`, `platform`, `created`) VALUES
(18, '111', 10, 0, 0, '', 'android', '2014-08-20 08:26:31'),
(19, '111', 13, 0, 0, '', 'android', '2014-08-20 08:26:35'),
(20, '111', 13, 0, 0, '', 'android', '2014-08-20 08:27:47'),
(21, '111', 14, 0, 0, '', 'android', '2014-08-20 08:27:50');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
